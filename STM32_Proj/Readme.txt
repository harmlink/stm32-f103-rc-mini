
PC.2  	----------------- 	LED0
PC.3   	-----------------	LED1
PC.1  	-----------------	BEEP

PA.9   -----------------  	Usart1  Tx
PA.10  -----------------  	Usart1  Rx

//PA.2   ----------------- 	Usart2  Tx
//PA.3   ----------------- 	Usart2  Rx

PB.10   -----------------  	Usart3  Tx
PB.11  -----------------  	Usart3  Rx

PC.10  -----------------     Uart4  Tx
PC.11  -----------------     Uart4  Rx

PC.12  -----------------     Uart5  Tx
PD.2  -----------------      Uart5  Rx


PC.4  -----------------  	ADC1  Channel 14
PC.5  -----------------  	ADC1  Channel 15

PA.2  -----------------     AT24C02 I2C SCL
PA.3  -----------------     AT24C02 I2C SDA


PA.0  -----------------  	KEY1
PC.13  -----------------  	KEY2


//PA.4  -----------------     DAC //(注意与DAC-OUT1 冲突，两者选其一)


PB.0  -----------------    IPS TFT SCL
PB.12  -----------------   IPS TFT SDA
PB.13  -----------------   IPS TFT RES
PB.14  -----------------   IPS TFT DC
PB.15  -----------------   IPS TFT CS

PA.4  -----------------   W25Q64 CS  //(注意与DAC-OUT1 冲突，两者选其一)
PA.5  -----------------   W25Q64 SCK
PA.6  -----------------   W25Q64 MISO
PA.7  -----------------   W25Q64 MOSI


//PC4  -----------------  	Exti 4
//PC5  -----------------  	Exti 5

PC.6  -----------------  	Timer3 CH1 PWM
PC.7  -----------------  	Timer3 CH2 PWM
 
PB.6  -----------------     Timer4 CH1 (In Capture 输入捕获)


PA.11  --------------- USB D-
PA.12  --------------- USB D+





#2023-9-27 解决nr_micro_shell利用串口调试助手接收不全问题(利用环形缓冲)。串口终端无问题
