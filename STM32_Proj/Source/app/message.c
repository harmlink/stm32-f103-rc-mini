/*******************************************************************************
 * @file    message.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __MESSAGE_C__


/* Includes ------------------------------------------------------------------*/
#include "message.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define MSG_POOL_CAPACITY 256

/* Private variables ---------------------------------------------------------*/
static uint8_t msg_pool[MSG_POOL_CAPACITY];//环形队列消息池


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/

struct rt_ringbuffer msg_ring_buf;//存储消息的环形队列,在system.c中初始化

/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
int Msg_Init(void)
{

	rt_ringbuffer_init(&msg_ring_buf, msg_pool, MSG_POOL_CAPACITY); //初始化消息环形队列

	return 0;

}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//static uint8_t message_buff[256];
SysMsg_TypeDef sys_msg;
void Msg_Process(void)  //系统消息处理
{
	if(rt_ringbuffer_data_len(&msg_ring_buf)!= 0) //消息队列不为空，则有消息需要处理
	{
		rt_ringbuffer_get(&msg_ring_buf,(uint8_t *) &sys_msg.MsgId, sizeof(SysMsg_TypeDef));//读取消息类型(1字节) + 消息参数(1字节)，其中：不同消息类型对应的消息参数意义也不同，由用户自行定义

	    
		switch(sys_msg.MsgId)
		{
			case MSG_UART1_REC_FINISH:
				/*
				rt_ringbuffer_get(&usart1_recv_ring_buf, message_buff, sys_msg.msg_parameter);
			    message_buff[sys_msg.msg_parameter] = '\0';//sys_msg.msg_parameter中存放的是串口1接收帧的长度信息
			    printf("%s\n",message_buff);
			    */
				break;
			
			default:
				break;
		}
		 
		
		
	}
	
	
}

/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
