/*******************************************************************************
 * @file    usb_mass_storage_app.h
 * @author  Harm
 * @version V1.00
 * @date    21-Oct-2023
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_MASS_STORAGE_APP_H__
#define __USB_MASS_STORAGE_APP_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __FATFS_APP_C__
#define EXTERN
#else
#define EXTERN extern
#endif



/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if ((FATFS_ENABLE) && (W25QXX_SFUD_ENABLE) &&(USB_MASS_STORAGE_ENABLE))
/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/
EXTERN void usb_mass_storage_init(void);

#endif


#ifdef __cplusplus
}
#endif



#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
