/*******************************************************************************
 * @file    fatfs_app.c
 * @author  Harm
 * @version V1.00
 * @date    21-Oct-2023
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __FATFS_APP_C__


/* Includes ------------------------------------------------------------------*/
#include "fatfs_app.h"

#if ((FATFS_ENABLE) && (W25QXX_SFUD_ENABLE))
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define ReadBufferSize 64
/* Private variables ---------------------------------------------------------*/
FATFS fs;					/* FatFs文件系统对象 */
FIL fobj;					/* 文件对象 */
UINT fnum;         			/* 文件成功读写数量 */
FRESULT f_res;     			/* 文件操作结果 */
char WriteBuffer[64]={0}; 	/*写缓冲区 */
char ReadBuffer[ReadBufferSize]={0};  	/*读缓冲区 */
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

void spi_flash_mkfs_and_mount(void)
{

//外部SPI FLASH挂载文件系统，文件系统挂载时会对SPI FLASH初始化
	f_res = f_mount(&fs,(TCHAR const*)"1:/",1);
  
    /*----------------------- 格式化测试 ---------------------------*/  
    /* 如果没有文件系统就格式化创建创建文件系统 */
    if(f_res == FR_NO_FILESYSTEM)
    {
		printf("---The SPI FLASH does not have a FAT system yet and will be formatted...\r\n");
		/* 格式化 */
		f_res=f_mkfs((TCHAR const*)"1:/",0,0);							
      
		if(f_res == FR_OK)
		{
			printf("---The SPI FLASH has successfully formatted---\r\n");
			/* 格式化后，先取消挂载 */
			f_res = f_mount(NULL,(TCHAR const*)"1:/",1);			
			/* 重新挂载	*/			
			f_res = f_mount(&fs,(TCHAR const*)"1:/",1);
			printf("---The SPIFLASH system is mounted successfully and can be read and written or del...\r\n");
		}
		else
		{
			printf("---Format fail---\r\n");
			while(1);
		}
	}
    else if(f_res!=FR_OK)
    {
		printf("！！SPI FLASH failed to mount (%d)\r\n",f_res);
		  //printf_fatfs_error(f_res);
		while(1);
    }
    else
    {
		printf("---The SPIFLASH system is mounted successfully and can be read and written or del...\r\n");
    }
    /*---------------------------------------------------------------------------*/	
	
}


static void Save_SensorData_To_SPIFlash(void)
{
	int i;

	printf("****** Store sensor data in SPI FLASH NOW!... ******\r\n");	

	//spi_flash_mkfs_and_mount();//格式化并挂载。该函数已在Sys_Init()中调用;
	                              //若已经格式化，则不会再次格式化
	
    /*----------------------- 文件系统测试：写测试 -----------------------------*/
    /* 打开文件，如果文件不存在则创建它 */
    printf("****** SPI FLASH write (Store sensor data) is about to take place... ******\r\n");	
    f_res = f_open(&fobj, "1:/sensor.csv", FA_OPEN_ALWAYS | FA_WRITE | FA_READ  );

    if ( f_res == FR_OK )
    {
		printf("---Open / create 1:/sensor.csv SPI FLASH File success, write data to SPI FLASH File---\r\n");
		/* 将指定存储区内容写入到文件内 */
		sprintf(WriteBuffer,"Temperature,Humidity\r\n");
		f_write(&fobj,WriteBuffer,strlen(WriteBuffer),&fnum);
		f_lseek (&fobj,f_size(&fobj));
		for(i=0;i<10;i++){
			sprintf(WriteBuffer,"%d,%d\r\n",i+20,i+70);//举例数据,可用实际传感器数据替代
			f_write(&fobj,WriteBuffer,strlen(WriteBuffer),&fnum);
		}  
		f_sync(&fobj);//直接同步，无须等等f_close之后才真正将数据写入SPI FLASH
		printf("Store sensor data in SPI FLASH successfully!.\r\n");
		/* 不再读写，关闭文件 */
		f_close(&fobj);
    }
    else
    {	
		printf("！！Failed to open 1:/sensor.csv create SPI FLASH File.\r\n");
    }
   
    
    /* 不再读写，关闭文件 */
	f_close(&fobj);
    
    /* 不再使用，取消挂载 */
	//f_mount(NULL,(TCHAR const*)"1:/",1);	//若spi_flash_mkfs_and_mount在Sys_Init()中调用，则无须取消挂载，系统允许期间一间挂载
	
   /*---------------------------------------------------------------------------*/
  
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

static void Read_SensorData_From_SPIFlash(void)
{

	 DWORD tempLocate = 0;
	 int   lineNum = 1;

	printf("****** Read sensor data in SPI FLASH NOW!... ******\r\n");	

	//spi_flash_mkfs_and_mount();//格式化并挂载。该函数已在Sys_Init()中调用;
	                              //若已经格式化，则不会再次格式化
	
    /*----------------------- 文件系统测试：读测试 -----------------------------*/
    /* 打开文件，如果文件不存在则创建它 */
    printf("****** SPI FLASH Read (Read sensor data) is about to take place... ******\r\n");	
    f_res = f_open(&fobj, "1:/sensor.csv", FA_OPEN_ALWAYS | FA_WRITE | FA_READ  );

    if ( f_res == FR_OK )
    {
		printf("---Open / create 1:/sensor.csv SPI FLASH File success.---\r\n");
		printf("The latest sensor data reading from SPI FLASH...\r\n");
		/* 将指定文件按行从存储区读出 */
		memset(ReadBuffer,0,ReadBufferSize);
		while(tempLocate < f_size(&fobj))
		{
			f_gets(ReadBuffer,ReadBufferSize,&fobj);
			printf("%d:%s\r\n",lineNum,ReadBuffer);
		
			tempLocate=f_tell(&fobj);
			//printf("locate is%d\r\n",tempLocate);
		
			f_lseek(&fobj, tempLocate);  
			memset(ReadBuffer,0,ReadBufferSize);
			
			lineNum++;
		}
	
		f_sync(&fobj);
		printf("Read sensor data from SPI FLASH successfully!.\r\n");

		/* 不再读写，关闭文件 */
		f_close(&fobj);
    }
    else
    {	
		printf("！！Failed to open 1:/sensor.csv create SPI FLASH File.\r\n");
    }
    
    
    /* 不再读写，关闭文件 */
	f_close(&fobj);
    
    /* 不再使用，取消挂载 */
	//f_mount(NULL,(TCHAR const*)"1:/",1);	//若spi_flash_mkfs_and_mount在Sys_Init()中调用，则无须取消挂载，系统允许期间一间挂载
   /*---------------------------------------------------------------------------*/
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
/**
 * @brief Save_SensorData_To_SPIFlash command
 */
void shell_Save_SensorData_To_SPIFlash_cmd(char argc, char *argv)
{
    Save_SensorData_To_SPIFlash();
}



/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
/**
 * @brief Read_SensorData_From_SPIFlash command
 */
void shell_Read_SensorData_From_SPIFlash_cmd(char argc, char *argv)
{
    Read_SensorData_From_SPIFlash();
}

void shell_Del_SensorData_SPIFlash_cmd(char argc, char *argv)
{
   

	printf("****** Del sensor data in SPI FLASH NOW!... ******\r\n");	

	//spi_flash_mkfs_and_mount();//格式化并挂载。该函数已在Sys_Init()中调用;
	                              //若已经格式化，则不会再次格式化
	

	f_unlink((TCHAR*) "1:/sensor.csv"); //删除传感器数据文件
	
	printf("delele 1:/sensor.csv successfully!\r\n");

}


#if 0

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

FRESULT scan_files_del(TCHAR* path)//递归删除文件
{
	FRESULT res;
	DIR dir;
	UINT i;
	static FILINFO fno;
 
	res = f_opendir(&dir, path);//打开此文件夹
	if (res == FR_OK){//成功打开文件夹
		for (;;){//循环扫描文件夹和文件
			res = f_readdir(&dir, &fno);//读取此文件家中的一个子文件
			if (res != FR_OK || fno.fname[0] == 0) break;//读取错误或者已经扫描完此文件夹下的所有文件，跳出循环
			if (fno.fattrib & AM_DIR) {//子文件为文件夹
				i = strlen(path);//统计附文件夹路径长度
				sprintf((char*)&path[i], "\\%s", fno.fname);//将子文件夹名加入路径
				res = scan_files_del(path);//递归进入子文件夹，扫描文件夹
				if (res != FR_OK) break;//操作失败跳出循环
				path[i] =0;//文件路径回退
			}
			else {//子文件为  文件类型
				i = strlen(path);//统计附文件夹路径长度
				sprintf((char*)&path[i], "\\%s", fno.fname);//将子文件名加入路径
				res=f_unlink(path);//删除子文件
				path[i] =0;//文件路径回退
			}
		}
		res=f_closedir(&dir);//挂你打开的父文件夹
		res=f_unlink(path);//删除已经清空的父文件夹
	}
	return res;
}

FRESULT f_deldir (TCHAR* path)//path为需要删除的文件夹
{
	FRESULT res;
	res = scan_files_del(path);
	return res;
}

#endif


#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
