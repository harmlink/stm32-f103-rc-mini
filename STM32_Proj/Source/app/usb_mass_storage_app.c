/*******************************************************************************
 * @file    usb_mass_storage_app.c
 * @author  Harm
 * @version V1.00
 * @date    21-Oct-2023
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __USB_MASS_STORAGE_APP_C__


/* Includes ------------------------------------------------------------------*/
#include "usb_mass_storage_app.h"
#include "hw_config.h" 
#include "usb_core.h"
#include "usb_init.h"


#if ((FATFS_ENABLE) && (W25QXX_SFUD_ENABLE) &&(USB_MASS_STORAGE_ENABLE))
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/

void usb_mass_storage_init(void) //harmlink usb
{	

	/*初始化*/
	Set_System();
  	
	/*设置USB时钟为48M*/
	Set_USBClock();
 	
//	/*配置USB中断(包括SDIO中断)*/
	USB_Interrupts_Config();
// 
//	/*USB初始化*/
 	USB_Init();

	
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
