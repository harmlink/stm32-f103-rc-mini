/*******************************************************************************
 * @file    task.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __TASK_C__


/* Includes ------------------------------------------------------------------*/
#include "task.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
LinkedList_TypeDef *head = NULL;


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       添加节点
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void LinkedList_AppendNode(TASK_InfoTypeDef info)
{
    LinkedList_TypeDef *node = head;

    if(head == NULL)
    {
        head = (LinkedList_TypeDef *)malloc(sizeof(LinkedList_TypeDef));

        if(head == NULL)
        {
            free(head);
        }
        else
        {
            memcpy(&head->info, &info, sizeof(TASK_InfoTypeDef));

            head->next = NULL;
        }
    }
    else
    {
        while(node->next != NULL)
        {
            node = node->next;
        }

        node->next = (LinkedList_TypeDef *)malloc(sizeof(LinkedList_TypeDef));

        if(node->next != NULL)
        {
            node = node->next;

            memcpy(&node->info, &info, sizeof(TASK_InfoTypeDef));

            node->next = NULL;
        }
        else
        {
            free(node->next);
        }
    }
}


/*******************************************************************************
 * @brief       查找节点
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
uint8_t LinkedList_SearchNode(uint8_t index)
{
    LinkedList_TypeDef *node = head;

    while(node != NULL)
    {
        if(node->info.index == index)
        {
            return 1;
        }

        node = node->next;
    }

    return 0;
}


/*******************************************************************************
 * @brief       添加任务
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Task_Append(uint8_t index, Task_Handler handler, uint32_t tick)
{
    TASK_InfoTypeDef info;

    info.index   = index;
    info.ready   = 0;
    info.tick    = tick;
    info.handler = handler;

    if(LinkedList_SearchNode(index) == 0)
    {
        LinkedList_AppendNode(info);
    }
}


/*******************************************************************************
 * @brief       任务时间片处理
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Task_TimeSlice(uint32_t tick)
{
    LinkedList_TypeDef *node = head;

    while(node != NULL)
    {
        if((tick % node->info.tick) == 0)
        {
            node->info.ready = 1;
        }
        
        node = node->next;
    }
}


/*******************************************************************************
 * @brief       任务调度
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Task_Scheduling(void)
{
    LinkedList_TypeDef *node = head;

    while(node != NULL)
    {
        if(node->info.ready)
        {
            node->info.ready = 0;
            node->info.handler();
        }

        node = node->next;
    }
}

/*******************************************************************************
 * @brief  添加周期性运行任务     
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void App_Task_Init(void)
{
/*
添加周期性任务步骤：
1.在task.h中定义任务ID宏
2.定义任务回调函数
3.在本函数内部利用Task_Append函数添加周期性任务	或者在任意函数内部调用Task_Append函数即可添加任务
  Task_Append(uint8_t index, Task_Handler handler, uint32_t tick);
*/
#if LED_ENABLE
	Task_Append(TASK_ID_LED, Led_Task_Handler, 500); 	//添加LED闪烁任务，回调函数Led_Toggle，周期1000ms
#endif

#if BEEP_ENABLE
	Task_Append(TASK_ID_BEEP, Beep_Task_Handler, 1000); 	//添加BEEP鸣停任务，回调函数Beep_Task_Handler，周期2000ms
#endif
	
#if USART1_ENABLE
	Task_Append(TASK_ID_USART1_RECV, Usart1Recv_Task_Handler, 10); 	//添加串口1接收处理任务，回调函数Usart1Recv_Task_Handler，周期10ms
#endif
	
#if USART2_ENABLE
	Task_Append(TASK_ID_USART2_RECV, Usart2Recv_Task_Handler, 20); 	//添加串口2接收处理任务，回调函数Usart2Recv_Task_Handler，周期20ms
#endif

#if USART3_ENABLE
	Task_Append(TASK_ID_USART3_RECV, Usart3Recv_Task_Handler, 20); 	//添加串口3接收处理任务，回调函数Usart3Recv_Task_Handler，周期20ms
#endif

#if UART4_ENABLE
	Task_Append(TASK_ID_UART4_RECV, Uart4Recv_Task_Handler, 20); 	//添加串口4接收处理任务，回调函数Uart4Recv_Task_Handler，周期20ms
#endif

#if UART5_ENABLE
	Task_Append(TASK_ID_UART5_RECV, Uart5Recv_Task_Handler, 20); 	//添加串口4接收处理任务，回调函数Uart4Recv_Task_Handler，周期20ms
#endif

#if ADC_ENABLE
	Task_Append(TASK_ID_ADC, ADC_Task_Handler, 5000); 	//添加ADC任务，回调函数ADC_Task_Handler，周期5000ms
#endif

#if DAC_ENABLE
	Task_Append(TASK_ID_DAC, Dac_Task_Handler, 10); // 添加DAC任务，输出按模拟量正弦变化
#endif

#if BUTTON_ENABLE	
	Task_Append(TASK_ID_BUTTON, button_ticks, 5);//添加按键任务，回调函数button_ticks，周期5ms
#endif

#if TIMER4_CAPTURE_ENABLE
	Task_Append(TASK_ID_TIMER4_RECV,Timer4_Capture_Task_Handler,3000); //添加输入捕获信息打印任务,3s周期，根据实际情况更改周期
#endif

#if XXX_ENABLE
    Task_Append(TASK_ID_INDEX,XXX_Task_Handler, tick);
	// 添加用户自定义任务，任务ID为TASK_ID_INDEX，回调函数XXX_Task_Handler，周期tick
#endif
}


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

