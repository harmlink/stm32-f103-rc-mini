/*******************************************************************************
 * @file    fatfs_app.h
 * @author  Harm
 * @version V1.00
 * @date    21-Oct-2023
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FATFS_APP_H__
#define __FATFS_APP_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __FATFS_APP_C__
#define EXTERN
#else
#define EXTERN extern
#endif



/* Includes ------------------------------------------------------------------*/
#include "config.h"
#include "ff.h"
#include "stdio.h"
#include "string.h"

#if ((FATFS_ENABLE) && (W25QXX_SFUD_ENABLE))
/* Exported constants --------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/
EXTERN void spi_flash_mkfs_and_mount(void);
EXTERN void shell_Save_SensorData_To_SPIFlash_cmd(char argc, char *argv);
EXTERN void shell_Read_SensorData_From_SPIFlash_cmd(char argc, char *argv);
EXTERN void shell_Del_SensorData_SPIFlash_cmd(char argc, char *argv);

#endif


#ifdef __cplusplus
}
#endif



#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
