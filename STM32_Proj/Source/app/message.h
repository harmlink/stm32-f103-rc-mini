/*******************************************************************************
 * @file    message.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MESSAGE_H__
#define __MESSAGE_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __MESSAGE_C__
#define EXTERN
#else
#define EXTERN extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"


/* Exported constants --------------------------------------------------------*/



/* Exported types ------------------------------------------------------------*/
typedef void(*Msg_Handler)(void);//消息回调函数类型


/* Exported types ------------------------------------------------------------*/
typedef enum //定义消息枚举类型
{
	MSG_NULL = 0,
	/******************************/
	//添加用户消息常量，例如：MSG_XXX,
	MSG_KEY1_PRESS,
	MSG_KEY2_PRESS,
	
	MSG_UART1_REC_FINISH,//串口1接收到一帧数据消息
	/******************************/
	MSG_NUM
	
}MsgId_TypeDef;

typedef struct 
{
	MsgId_TypeDef 	MsgId;         	//系统消息类型，比如：MSG_USART1_REC_FINISH，代表串口1接收一帧数据结束
	unsigned char 	Msg_Parameter; 	//系统消息参数，比如：当消息类型为MSG_USART1_REC_FINISH，msg_parameter代表帧长度
	Msg_Handler 	Msg_Handler;   	//消息处理回调函数
}SysMsg_TypeDef;

/* Exported types ------------------------------------------------------------*/

/* Exported variables --------------------------------------------------------*/
extern struct rt_ringbuffer msg_ring_buf;

/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
int 	Msg_Init(void);      	//系统消息初始化 
void 	Msg_Process(void);  	//系统消息处理

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
