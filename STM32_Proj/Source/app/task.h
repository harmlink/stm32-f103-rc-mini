/*******************************************************************************
 * @file    task.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TASK_H__
#define __TASK_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __TASK_C__
#define EXTERN
#else
#define EXTERN extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"


/* Exported constants --------------------------------------------------------*/
#define TASK_ID_BUTTON    		0
#define TASK_ID_LED     		1
#define TASK_ID_BEEP        	2
#define TASK_ID_USART2_RECV		3
#define TASK_ID_USART3_RECV		4
#define	TASK_ID_ADC         	5
#define TASK_ID_DAC				6
#define TASK_ID_RTC				7
#define TASK_ID_USART1_RECV		8
#define TASK_ID_UART4_RECV      9
#define TASK_ID_UART5_RECV      10
#define TASK_ID_TIMER4_RECV     11

/* Exported types ------------------------------------------------------------*/
typedef void(*Task_Handler)(void);//任务回调函数类型


/* Exported types ------------------------------------------------------------*/
typedef struct
{
    uint8_t  index;  		//任务ID
    uint8_t  ready;   		//任务是否就绪
    uint32_t tick;			//任务运行周期
    Task_Handler handler;   //任务回调函数
} TASK_InfoTypeDef;


/* Exported types ------------------------------------------------------------*/
typedef struct _Node_Struct
{
    TASK_InfoTypeDef info;
    struct _Node_Struct *next;
} LinkedList_TypeDef;   //任务链表类型


/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
EXTERN void Task_Append(uint8_t index, Task_Handler handler, uint32_t tick);
EXTERN void Task_TimeSlice(uint32_t tick);
EXTERN void Task_Scheduling(void);
EXTERN void App_Task_Init(void);

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
