/**
  ******************************************************************************
  * @file    mass_mal.c
  * @author  MCD Application Team
  * @version V3.4.0
  * @date    29-June-2012
  * @brief   Medium Access Layer interface
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/


//#include "./flash/bsp_spi_flash.h"
#include "sfud.h"
#include "mass_mal.h"
#include <stdio.h>
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t Mass_Memory_Size[2];
uint32_t Mass_Block_Size[2];
uint32_t Mass_Block_Count[2];
__IO uint32_t Status = 0;
//#define  sFLASH_ID              0xEF3015     //W25X16
//#define  sFLASH_ID              0xEF4015	 //W25Q16
#define  sFLASH_ID              0XEF4017    //W25Q64

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : MAL_Init
* Description    : Initializes the Media on the STM32
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
uint16_t MAL_Init(uint8_t lun)
{
  extern bool sfud_read_sfdp(sfud_flash *flash);
  uint16_t status = MAL_OK;
  sfud_flash *flash;

  switch (lun)
  {
    case 0:
			//FLASH_SPI_disk_initialize();
		sfud_device_init(sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX));
		flash = sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX);;
		sfud_read_sfdp(flash);
		if(((flash->chip).type_id == 0x40) &&((flash->chip).capacity_id == 0x17))//(SPI_FLASH_ReadID()==sFLASH_ID)
		{
//				printf("flash init succsee\n");
			Status = MAL_OK;
		}
		else
			Status = MAL_FAIL;
		break;
    default:
		return MAL_FAIL;
  }
  return status;
}
/*******************************************************************************
* Function Name  : MAL_Write
* Description    : Write sectors
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
uint16_t MAL_Write(uint8_t lun, uint32_t Memory_Offset, uint32_t *Writebuff, uint16_t Transfer_Length)
{
  switch (lun)
  {
    case 0:
			Memory_Offset+=(514*4096);//扇区偏移，外部Flash文件系统空间放在外部Flash后面6M空间
			//printf("write add =%d.length=%d\n",Memory_Offset/4096,Transfer_Length/4096);
			//SPI_FLASH_SectorErase(Memory_Offset);
			//SPI_FLASH_BufferWrite((uint8_t *)Writebuff,Memory_Offset,Transfer_Length);
			sfud_erase(sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX), Memory_Offset, Transfer_Length);
			sfud_write(sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX), Memory_Offset, Transfer_Length, (u8 *)Writebuff);
      break;
    default:
      return MAL_FAIL;
  }
  return MAL_OK;
}

/*******************************************************************************
* Function Name  : MAL_Read
* Description    : Read sectors
* Input          : None
* Output         : None
* Return         : Buffer pointer
*******************************************************************************/
uint16_t MAL_Read(uint8_t lun, uint32_t Memory_Offset, uint32_t *Readbuff, uint16_t Transfer_Length)
{

  switch (lun)
  {
    case 0:
			Memory_Offset+=(514*4096);//扇区偏移，外部Flash文件系统空间放在外部Flash后面6M空间
			//printf("read add =%d.length=%d\n",Memory_Offset/4096,Transfer_Length/4096);
//			SPI_FLASH_BufferRead((uint8_t *)Readbuff, Memory_Offset, Transfer_Length);
		    sfud_read(sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX),Memory_Offset, Transfer_Length,(uint8_t *) Readbuff);
      break;
    default:
      return MAL_FAIL;
  }
  return MAL_OK;
}

/*******************************************************************************
* Function Name  : MAL_GetStatus
* Description    : Get status
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
uint16_t MAL_GetStatus (uint8_t lun)
{
  extern bool sfud_read_sfdp(sfud_flash *flash);
  sfud_flash *flash;
  
  if (lun == 0)
  {
    //FLASH_SPI_disk_initialize();
	//sfud_device_init(sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX));
	flash = sfud_get_device(SFUD_W25Q64CV_DEVICE_INDEX);;
	sfud_read_sfdp(flash);
	if(((flash->chip).type_id == 0x40) &&((flash->chip).capacity_id == 0x17))//(SPI_FLASH_ReadID()==sFLASH_ID)
    {
			Mass_Block_Size[0]  =4096;
			Mass_Block_Count[0] =1534;
			Mass_Memory_Size[0] =Mass_Block_Size[0]*Mass_Block_Count[0];
			return MAL_OK;
    }
  }
  return MAL_FAIL;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

