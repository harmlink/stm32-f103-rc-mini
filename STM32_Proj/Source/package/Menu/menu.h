#ifndef __MENU_H
#define __MENU_H	 
#include "config.h"                  // Device header


typedef struct
{
    u8 current;//当前状态索引号
    u8 next; //向下一个
    u8 enter; //确定
    void (*current_operation)(void); //当前状态应该执行的操作
} Menu_table;

extern Menu_table  menu_table[26];

void Menu_key_set(void);
void MainPage(void);
void SecondPageChoice01(void);
void SecondPageChoice02(void);
void SecondPageChoice03(void);
void SecondPageChoiceReturn(void);

void Choice01_Return(void);
void Choice01_Item01(void);
void Choice01_Item02(void);
void Choice01_Item03(void);

void Choice02_Return(void);
void Choice02_Item01(void);
void Choice02_Item02(void);
void Choice02_Item03(void);

void Choice03_Return(void);
void Choice03_Item01(void);
void Choice03_Item02(void);
void Choice03_Item03(void);

void Choice01_Item01_Content(void);
void Choice01_Item02_Content(void);
void Choice01_Item03_Content(void);
void Choice02_Item01_Content(void);
void Choice02_Item02_Content(void);
void Choice02_Item03_Content(void);
void Choice03_Item01_Content(void);
void Choice03_Item02_Content(void);
void Choice03_Item03_Content(void);
#endif	
