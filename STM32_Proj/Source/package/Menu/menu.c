#include "menu.h"
#include "stm32f10x.h"                  // Device header


//void (*current_operation_index)(); //执行当前操作函数
//uint8_t func_index = 0;
//uint8_t key1;
//uint8_t key2;

/*定义了索引数组，索引数组要配合主程序按键值进行选择*/
 Menu_table  menu_table[26]=
{
    {0,0,1,(*MainPage)},//一级界面
	
    {1,2, 5,(*SecondPageChoice01)},//二级界面    第一行
    {2,3, 9,(*SecondPageChoice02)},//二级界面    第二行
    {3,4,13,(*SecondPageChoice03)},//二级界面   第三行
    {4,1, 0,(*SecondPageChoiceReturn)},//三级界面    Back
	
    {5, 6, 4, (*Choice01_Return)},  //三级菜单 Back
    {6, 7, 17, (*Choice01_Item01)}, //三级菜单 
    {7, 8, 18, (*Choice01_Item02)}, //三级菜单 
    {8, 5, 19, (*Choice01_Item03)}, //三级菜单 

    {9 , 10, 4, (*Choice02_Return)},    //三级菜单  Back
    {10, 11, 20, (*Choice02_Item01)}, //三级菜单 
    {11, 12, 21, (*Choice02_Item02)}, //三级菜单 
    {12, 9 , 22, (*Choice02_Item03)},  //三级菜单 

    {13, 14, 4, (*Choice03_Return)},  //三级菜单  Back
    {14, 15, 23, (*Choice03_Item01)}, //三级菜单 
    {15, 16, 24, (*Choice03_Item02)}, //三级菜单 
    {16, 13, 25, (*Choice03_Item03)}, //三级菜单 
	
	{17, 17, 6, (*Choice01_Item01_Content)}, //四级内容
    {18, 18, 7, (*Choice01_Item02_Content)}, //四级内容
    {19, 19, 8, (*Choice01_Item03_Content)}, //四级内容
	
	{20, 20, 10, (*Choice02_Item01_Content)}, //四级内容
    {21, 21, 11, (*Choice02_Item02_Content)}, //四级内容
    {22, 22, 12, (*Choice02_Item03_Content)}, //四级内容
	
	{23, 23, 14, (*Choice03_Item01_Content)}, //四级内容
    {24, 24, 15, (*Choice03_Item02_Content)}, //四级内容
    {25, 25, 16, (*Choice03_Item03_Content)}, //四级内容
	
};

/*
void  Menu_key_set(void)
{
  key1 = KEY1_Get();
  key2 = KEY2_Get();
  if(key1 == 1)
  {   
    func_index=table[func_index].next;//按键next按下后的索引号
    //OLED_Clear();
  }
  if(key2 == 1)
  {
    func_index=table[func_index].enter;
   // OLED_Clear();
  }

  current_operation_index=table[func_index].current_operation;//执行当前索引号所对应的功能函数。
  (*current_operation_index)();//执行当前操作函数
}
*/

void MainPage(void)//第一页
{
	printf("/****************/\r\n");
    printf("Menu Test\r\n");
	printf("KEY1 SINGLE_CLICK <--> Next Item\r\n");
	printf("KEY1 DOUBLE_CLICK <--> ENTER Item\r\n");
	printf("KEY1 LONGPRESS_CLICK <--> Back to MainPage\r\n");
	printf("/****************/\r\n");
	
#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_Test();
#endif
}


void SecondPageChoice01(void)//第二页
{
	printf("/****************/\r\n");
	printf("Test 1   <\r\n");
	printf("Test 2\r\n");
	printf("Test 3\r\n");
	printf("Back \r\n");
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Test 1   <     ",RED);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2         ",BLUE); 
	LCD_ShowString(10,60,(unsigned char *)"  Test 3         ",BLUE);
	LCD_ShowString(10,80,(unsigned char *)"  Back           ",BLUE);  	
#endif
}


void SecondPageChoice02(void)
{
	printf("/****************/\r\n");
	printf("Test 1\r\n");
	printf("Test 2   <\r\n");
	printf("Test 3\r\n");
	printf("Back \r\n"); 
	printf("/****************/\r\n");	
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Test 1         ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2   <     ",RED); 
	LCD_ShowString(10,60,(unsigned char *)"  Test 3         ",BLUE);
	LCD_ShowString(10,80,(unsigned char *)"  Back           ",BLUE);  	
#endif
}

void SecondPageChoice03(void)
{
	printf("/****************/\r\n");
	printf("Test 1\r\n");
	printf("Test 2\r\n");
	printf("Test 3   <\r\n");
	printf("Back \r\n");
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Test 1         ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2         ",BLUE); 
	LCD_ShowString(10,60,(unsigned char *)"  Test 3   <     ",RED);
	LCD_ShowString(10,80,(unsigned char *)"  Back           ",BLUE);  	
#endif
}

void SecondPageChoiceReturn(void)
{
	printf("/****************/\r\n");
	printf("Test 1\r\n");
	printf("Test 2\r\n");
	printf("Test 3\r\n");
	printf("Back     < \r\n");  
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Test 1         ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2         ",BLUE); 
	LCD_ShowString(10,60,(unsigned char *)"  Test 3         ",BLUE);
	LCD_ShowString(10,80,(unsigned char *)"  Back     <     ",RED);  	
#endif
}

void Choice01_Return(void)//第二页第一项对应的第三页
{
	printf("/****************/\r\n");
	printf("Back     < \r\n"); 
	printf("Test 1.1\r\n");
	printf("Test 1.2\r\n");
	printf("Test 1.3\r\n");
	printf("/****************/\r\n");	
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back      <     ",RED);
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.1        ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 1.2        ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 1.3        ",BLUE);  	
#endif
}

void Choice01_Item01(void)
{
	printf("/****************/\r\n");
	printf("Back    \r\n"); 
	printf("Test 1.1 < \r\n");
	printf("Test 1.2\r\n");
	printf("Test 1.3\r\n"); 
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back            ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.1  <     ",RED);
	LCD_ShowString(10,60,(unsigned char *)"  Test 1.2        ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 1.3        ",BLUE);  	
#endif
}

void Choice01_Item02(void)
{
	printf("/****************/\r\n");
	printf("Back    \r\n"); 
	printf("Test 1.1\r\n");
	printf("Test 1.2 < \r\n");
	printf("Test 1.3\r\n");
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 1.2   <     ",RED); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 1.3         ",BLUE);  	
#endif
}

void Choice01_Item03(void)
{
	printf("/****************/\r\n");
	printf("Back    \r\n"); 
	printf("Test 1.1\r\n");
	printf("Test 1.2\r\n");
	printf("Test 1.3 < \r\n");
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 1.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 1.3   <     ",RED);  	
#endif
}

void Choice02_Return(void)//第二页第二项对应的第三页
{
	printf("/****************/\r\n");
	printf("Back     < \r\n"); 
	printf("Test 2.1\r\n");
	printf("Test 2.2\r\n");
	printf("Test 2.3\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back       <     ",RED);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 2.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 2.3         ",BLUE);  	
#endif
}

void Choice02_Item01(void)
{
	printf("/****************/\r\n");
	printf("Back\r\n");
	printf("Test 2.1 < \r\n");
	printf("Test 2.2\r\n");
	printf("Test 2.3\r\n"); 
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.1   <     ",RED);
	LCD_ShowString(10,60,(unsigned char *)"  Test 2.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 2.3         ",BLUE);  	
#endif
}

void Choice02_Item02(void)
{
	printf("/****************/\r\n");
	printf("Back\r\n");
	printf("Test 2.1\r\n");
	printf("Test 2.2 < \r\n");
	printf("Test 2.3\r\n"); 
	printf("/****************/\r\n");
	
#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 2.2   <     ",RED); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 2.3         ",BLUE);  	
#endif
}

void Choice02_Item03(void)
{
	printf("/****************/\r\n");
	printf("Back\r\n");
	printf("Test 2.1\r\n");
	printf("Test 2.2\r\n");
	printf("Test 2.3 < \r\n"); 
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 2.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 2.3   <     ",RED);  	
#endif
}

void Choice03_Return(void)//第二页第三项对应的第三页
{
	printf("/****************/\r\n");
	printf("Back     < \r\n"); 
	printf("Test 3.1\r\n");
	printf("Test 3.2\r\n");
	printf("Test 3.3\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back       <     ",RED);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 3.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 3.3         ",BLUE);  	
#endif
}

void Choice03_Item01(void)
{
	printf("/****************/\r\n");
	printf("Back    \r\n"); 
	printf("Test 3.1 < \r\n");
	printf("Test 3.2\r\n");
	printf("Test 3.3\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.1   <     ",RED);
	LCD_ShowString(10,60,(unsigned char *)"  Test 3.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 3.3         ",BLUE);  	
#endif
}

void Choice03_Item02(void)
{
	printf("/****************/\r\n");
	printf("Back    \r\n"); 
	printf("Test 3.1\r\n");
	printf("Test 3.2 < \r\n");
	printf("Test 3.3\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 3.2   <     ",RED); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 3.3         ",BLUE);  	
#endif
}

void Choice03_Item03(void)
{
	printf("/****************/\r\n");
	printf("Back    \r\n"); 
	printf("Test 3.1\r\n");
	printf("Test 3.2\r\n");
	printf("Test 3.3 < \r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
//	LCD_Clear(WHITE);
	LCD_ShowString(10,20,(unsigned char *)"  Back             ",BLUE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.1         ",BLUE);
	LCD_ShowString(10,60,(unsigned char *)"  Test 3.2         ",BLUE); 
	LCD_ShowString(10,80,(unsigned char *)"  Test 3.3   <     ",RED);  	
#endif
    
}
void Choice01_Item01_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 1.1 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);	
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.1 content   ",BLUE);  	
#endif
}
void Choice01_Item02_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 1.2 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.2 content   ",BLUE);  	
#endif
}
void Choice01_Item03_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 1.3 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 1.3 content   ",BLUE);  	
#endif
}
void Choice02_Item01_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 2.1 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.1 content   ",BLUE);  	
#endif
}
void Choice02_Item02_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 2.2 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.2 content   ",BLUE);  	
#endif
}
void Choice02_Item03_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 2.3 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 2.3 content   ",BLUE);  	
#endif
}
void Choice03_Item01_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 3.1 content\r\n");
	printf("/****************/\r\n");
	
#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.1 content   ",BLUE);  	
#endif
}
void Choice03_Item02_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 3.2 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.2 content   ",BLUE);  	
#endif
}
void Choice03_Item03_Content(void)
{
	printf("/****************/\r\n");
	printf("Test 3.3 content\r\n");
	printf("/****************/\r\n");

#if LCD_ENABLE
	LCD_Clear(WHITE);
	LCD_ShowString(10,40,(unsigned char *)"  Test 3.3 content   ",BLUE);  	
#endif
}
