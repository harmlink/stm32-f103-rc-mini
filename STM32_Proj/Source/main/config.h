#ifndef  __CONFIG_H__
#define  __CONFIG_H__

//无符号类型的定义
#define uchar unsigned char
#define uint unsigned int 
	
#define uint8 unsigned char
#define uint16 unsigned short int
#define uint32 unsigned int
	
//----------------------------------------


//全局宏定义
#define CPU_INT_DISABLE()  {__set_PRIMASK(1);} //关闭中断
#define CPU_INT_ENABLE()   {__set_PRIMASK(0);}  //打开中断

/* Exported constants 用于配置外设模块-------------------*/
#define USART1_ENABLE   		1  //调试串口，默认使能
#define ADC_ENABLE      		1
#define CAN_ENABLE      		0
#define BUTTON_ENABLE   		1
#define LCD_ENABLE      		1 //中景园 1.3寸 IPS TFT液晶屏 ST7789
#define LED_ENABLE      		1
#define BEEP_ENABLE     		1
#define OLED_ENABLE     		0
#define PWM_ENABLE      		1
#define RTC_ENABLE      		0 
#define INFLASH_ENABLE  		1 //根据代码占用空间情况，决定是否开启内部剩余Flash空间共用户使用
#define USART2_ENABLE   		0 //野火STM32F103-MINI开发板串口2与AT24C02 I2C接口冲突(PA.2、PA.3) 
#define USART3_ENABLE   		1 
#define TIMER2_ENABLE   		0 
#define UART4_ENABLE    		1 
#define UART5_ENABLE    		1 
#define	I2C_ENABLE				1
#define AT24C02_ENABLE  		1 //使用AT24C02C，必须使能I2C_ENABLE、同时禁止USART2功能(因为引脚冲突，除非更改引脚)
#define MENU_ENABLE     		1   
#define DAC_ENABLE      		0  //野火STM32F103-MINI开发板DAC-OUT1(PA.4)与W25Q64的CS引脚(PA.4)冲突,两者选其一
#define W25QXX_SFUD_ENABLE 		1  //野火STM32F103-MINI开发板W25Q64的CS引脚(固定PA.4)与DAC-OUT1(PA.4)冲突,两者选其一
#define FATFS_ENABLE        	1
#define USB_MASS_STORAGE_ENABLE 1
#define TIMER4_CAPTURE_ENABLE   1

//全局类型定义
typedef enum{FALSE = 0, TRUE = !FALSE}BOOL;
	
//-------------------------------------
#ifdef MAIN_CONFIG  //头文件被多个C调用时，避免变量冲突问题
	#define EXT
#else 
	#define EXT extern
#endif
	
	
	
	
//-------------------------------------
	
//常用头文件包含
#include "stm32f10x.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>	
#include "task.h"
#include "system.h"	
#include "multi_button.h"
#include "ringbuffer.h"
#include "message.h"
#include "sfud.h"
#include "fatfs_app.h"

//-------------------------------------
	
//外设驱动头文件包含
#include "bsp_systick.h"
#include "bsp_led.h"
#include "bsp_beep.h"
#include "bsp_usart1.h"
#include "bsp_usart2.h"
#include "bsp_usart3.h"	
#include "bsp_uart4.h"	
#include "bsp_uart5.h"	
#include "bsp_adc.h"	
#include "bsp_dac.h"
#include "bsp_button.h"
#include "bsp_timer2.h"
#include "bsp_inflash.h"
#include "bsp_pwm.h"
#include "bsp_i2c.h"
#include "bsp_at24c02.h"
#include "menu.h"
#include "bsp_lcd.h"
#include "bsp_spi.h"
#include "bsp_w25qxx.h"
#include "fatfs_app.h"
#include "usb_mass_storage_app.h"
#include "bsp_timer4.h"  //in capture
//----------------------------------------
//全局变量定义,请使用EXT修饰
EXT unsigned char g_Var;	

//----------------------------------------	
#endif

/********************************************************************************************************
**                            End Of File
********************************************************************************************************/

