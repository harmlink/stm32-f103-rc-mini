/*******************************************************************************
 * @file    system.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __SYSTEM_C__


/* Includes ------------------------------------------------------------------*/
#include "system.h"
#include "nr_micro_shell.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/



/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Cpu_Init(void)
{
	//CPU相关设置
	
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//选择第2组中断优先级，即2位主优先级，2位次优先级
	
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Sys_Init(void)
{
   Cpu_Init();
   SysTick_Init();//任务定时器、毫秒、微妙初始化
	
/*-------------用户添加外设初始化函数----BEGIN------*/	
#if LED_ENABLE
	Led_Init();
#endif
	
#if BEEP_ENABLE
	Beep_Init();
#endif

#if USART1_ENABLE
	Usart1_Init();
#endif
	
#if USART2_ENABLE
	Usart2_Init();
#endif

#if USART3_ENABLE
	Usart3_Init();
	Usart3_SendString((unsigned char*)"uart3 init\r\n");
#endif

#if ADC_ENABLE
	Adc_Init();
#endif

#if DAC_ENABLE
	Dac_Init();
#endif

#if BUTTON_ENABLE
	Button_Init();
#endif

#if TIMER2_ENABLE
	Timer2_Init(7199,10000);//1s中断一次，具体实现参见中断回调函数TIM2_IRQHandler
#endif

#if PWM_ENABLE
PWM_Init();//Time3,CH1:PC.6,CH2:PC7.10k PWM 开机占空比分别为50%,40%.注意：因板子资源，引脚重映射了
#endif

#if UART4_ENABLE
	Uart4_Init();
	Uart4_SendString((unsigned char*)"uart4 init\r\n");
#endif

#if UART5_ENABLE
	Uart5_Init();
	Uart5_SendString((unsigned char*)"uart5 init\r\n");
#endif

#if	I2C_ENABLE		
	I2cGpioInit();
#endif

#if AT24C02_ENABLE  
	At24c02Init();
	AT24C02Test();//开机测试AT24C02
#endif

#if MENU_ENABLE
	MainPage();//串口显示，按键控制，模拟菜单
#endif

#if LCD_ENABLE
	Lcd_Init();			//初始化OLED  
	LCD_Clear(WHITE);
	BACK_COLOR=WHITE;
	LCD_Test();
#endif

#if W25QXX_SFUD_ENABLE
	sfud_init();//SPI1 W25Q64 SFUD
	w25qxx_test();//测试
#endif

#if ((FATFS_ENABLE) && (W25QXX_SFUD_ENABLE))
	spi_flash_mkfs_and_mount();
#endif

#if ((FATFS_ENABLE) && (W25QXX_SFUD_ENABLE) &&(USB_MASS_STORAGE_ENABLE))
	usb_mass_storage_init();
#endif

#if INFLASH_ENABLE
    //无需初始化，直接使用以下两个函数进行读写
	//StmInFlash_ReadBytes , StmInFlash_WriteBytes
	//本例程在shell命令行中使用flashr flashw两个命令进行测试
#endif

#if TIMER4_CAPTURE_ENABLE
	Tim4Init();//输入捕获定时器及相关通道初始化
#endif

#if XX_ENABLE
//添加其他外设初始化函数
#endif 
	
/*-------------用户添加外设初始化函数----END------*/

/********************************************/	
	App_Task_Init();
/********************************************/	
}


void Sys_Run(void)
{
	
	Task_Scheduling();
	Msg_Process();
	


}

/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

