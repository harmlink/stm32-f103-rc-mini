/*******************************************************************************
 * @file    bsp_led.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_LED_H__
#define __BSP_LED_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_LED_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if LED_ENABLE

/* Exported constants --------------------------------------------------------*/



/* Exported types ------------------------------------------------------------*/
typedef struct
{
    GPIO_TypeDef *GPIOx;
    uint32_t      pin;
} LED_TypeDef;


/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
EXTERN void Led_Init(void);
EXTERN void Led_Task_Handler(void);
EXTERN void Led_Shell_Handler(uint8_t Enable);

#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

