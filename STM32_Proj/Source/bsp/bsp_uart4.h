/*******************************************************************************
 * @file    bsp_uart4.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_UART4_H__
#define __BSP_UART4_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_UART4_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if UART4_ENABLE

/* Exported constants --------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/



/* Exported macro ------------------------------------------------------------*/
#define UART4_BAUD         115200
#define UART4_RX_BUF_SIZE  256

/* Exported variables --------------------------------------------------------*/
EXTERN uint8_t   	g_UART4_RxBuf[UART4_RX_BUF_SIZE];//串口接收缓冲区
EXTERN uint16_t 	g_UART4_RecPos;//存放当前串口接收数据存放的位置

/* Exported functions --------------------------------------------------------*/

EXTERN void Uart4_Init(void);
EXTERN void Uart4_SendString(uint8_t *str);//发送字符串
EXTERN void Uart4Recv_Task_Handler(void);

#endif

#ifdef __cplusplus
}
#endif


//void u4_printf(char *fmt, ...);
#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

