#ifndef __BSP_W25QXX_H__
#define	__BSP_W25QXX_H__
#include "config.h"
#include "stm32f10x.h"
#include "bsp_spi.h"

void w25qxx_spix_init(void);
void w25qxx_test(void);
#endif /* __BSP_W25QXX_H__*/


/*********************************END OF FILE****************************************/
