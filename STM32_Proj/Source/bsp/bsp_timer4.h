/*******************************************************************************
 * @file    bsp_timer4.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_TIMER4_H__
#define __BSP_TIMER4_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_TIMER2_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if TIMER4_CAPTURE_ENABLE

/* Exported constants --------------------------------------------------------*/

#define GENERAL_TIM4_Period    0xffff
#define GENERAL_TIM4_Prescaler  72

/* Exported types ------------------------------------------------------------*/
typedef struct
{
	FlagStatus Egde;//Egde = SET表示当前处于高电平;Egde = RESET表示当前处于低电平;
	uint8 ucFinishFlag;//捕获结束标记
	uint16 usCaptureRisingVal;//输入捕获值
	uint16 usUpdateCnt;//计数溢出次数
	uint32 ulFrequency;//输入波形的频率
	
}WaveCapture;

/* Exported macro ------------------------------------------------------------*/


/* Exported variable ------------------------------------------------------------*/
extern WaveCapture waveCapture;

/* Exported functions --------------------------------------------------------*/
EXTERN void Tim4Init(void);//Timer4配置为输入捕获模式，产生1个通道输入捕获，可扩展
EXTERN void Timer4_Capture_Task_Handler(void);

#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

