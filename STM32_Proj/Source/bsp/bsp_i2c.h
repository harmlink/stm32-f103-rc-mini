/*******************************************************************************
 * @file    bsp_i2c.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __BSP_I2C_H__
#define __BSP_I2C_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_I2C_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"


#if I2C_ENABLE
/* Exported constants --------------------------------------------------------*/
#define I2C_SCL_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define I2C_SCL_APBxClock_CLK    	RCC_APB2Periph_GPIOA		
#define I2C_SCL_GPIO_PORT GPIOA
#define I2C_SCL_GPIO_PIN  GPIO_Pin_2

#define I2C_SDA_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define I2C_SDA_APBxClock_CLK    	RCC_APB2Periph_GPIOA	
#define I2C_SDA_GPIO_PORT GPIOA
#define I2C_SDA_GPIO_PIN  GPIO_Pin_3

#define I2C_SCL_LOW    GPIO_ResetBits(I2C_SCL_GPIO_PORT,I2C_SCL_GPIO_PIN)
#define I2C_SCL_HIGH   GPIO_SetBits(I2C_SCL_GPIO_PORT,I2C_SCL_GPIO_PIN)


#define I2C_SDA_LOW    GPIO_ResetBits(I2C_SDA_GPIO_PORT,I2C_SDA_GPIO_PIN)
#define I2C_SDA_HIGH   GPIO_SetBits(I2C_SDA_GPIO_PORT,I2C_SDA_GPIO_PIN)


#define I2C_SDA_READ   GPIO_ReadInputDataBit(I2C_SDA_GPIO_PORT,I2C_SDA_GPIO_PIN)

#define I2C_ACK   0
#define I2C_NOACK 1

/* Exported types ------------------------------------------------------------*/


/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/

EXTERN void I2cGpioInit(void);
EXTERN void I2cStart(void);
EXTERN void I2cStop(void);
EXTERN void I2cAck(void);
EXTERN void I2cNoAck(void);
EXTERN unsigned char I2cReadAck(void);
EXTERN void I2cWriteByte(unsigned char ByteData);
EXTERN void I2cReadByte(unsigned char *ByteData);

#endif

#ifdef __cplusplus
}
#endif

#endif



