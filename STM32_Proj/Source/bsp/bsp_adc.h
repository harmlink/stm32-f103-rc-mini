/*******************************************************************************
 * @file    bsp_adc.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_ADC_H__
#define __BSP_ADC_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_ADC_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if ADC_ENABLE

/* Exported constants --------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/
#define NbrOfChannel 2

/* Exported variables --------------------------------------------------------*/
EXTERN __IO uint16 ADCConvertedValue[NbrOfChannel];

/* Exported functions --------------------------------------------------------*/
EXTERN void Adc_Init(void);
EXTERN void ADC_Task_Handler(void);
EXTERN void shell_ADC_Test_cmd(char argc, char *argv);
#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

