#include "config.h"
#include "Timer.h"

#if SPWM_EN ==1
static  uint8 bIsPostiveHalf = 0;
static  uint16 SineTablePos = 0;
static  uint16 DutyCycleSineWaveTable[SamplesPerHalfCycle]=
{
	0x0000,0x0064,0x00C8,0x012D,0x0191,0x01F5,0x0259,0x02BC,0x031F,0x0381,0x03E3,0x0444,0x04A5,0x0504,0x0563,0x05C2,
	0x061F,0x067B,0x06D7,0x0731,0x078A,0x07E2,0x0839,0x088F,0x08E3,0x0936,0x0987,0x09D7,0x0A26,0x0A73,0x0ABE,0x0B08,
	0x0B50,0x0B96,0x0BDA,0x0C1D,0x0C5E,0x0C9D,0x0CD9,0x0D14,0x0D4D,0x0D84,0x0DB9,0x0DEB,0x0E1C,0x0E4A,0x0E76,0x0EA0,
	0x0EC8,0x0EED,0x0F10,0x0F31,0x0F4F,0x0F6B,0x0F85,0x0F9C,0x0FB1,0x0FC3,0x0FD3,0x0FE1,0x0FEC,0x0FF4,0x0FFB,0x0FFE,
	0x1000,0x0FFE,0x0FFB,0x0FF4,0x0FEC,0x0FE1,0x0FD3,0x0FC3,0x0FB1,0x0F9C,0x0F85,0x0F6B,0x0F4F,0x0F31,0x0F10,0x0EED,
	0x0EC8,0x0EA0,0x0E76,0x0E4A,0x0E1C,0x0DEB,0x0DB9,0x0D84,0x0D4D,0x0D14,0x0CD9,0x0C9D,0x0C5E,0x0C1D,0x0BDA,0x0B96,
	0x0B50,0x0B08,0x0ABE,0x0A73,0x0A26,0x09D7,0x0987,0x0936,0x08E3,0x088F,0x0839,0x07E2,0x078A,0x0731,0x06D7,0x067B,
	0x061F,0x05C2,0x0563,0x0504,0x04A5,0x0444,0x03E3,0x0381,0x031F,0x02BC,0x0259,0x01F5,0x0191,0x012D,0x00C8,0x0064
};


#endif 


//-------------------定时器2相关开始------------------------------------------------------//
//演示普通定时功能

void Tim2ModeInit(int prescaler,int period)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
	
	TIM_DeInit(TIM2);										//重新将Timer设置为缺省值
	TIM_InternalClockConfig(TIM2);							//采用内部时钟给TIM2提供时钟源
	TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t)(prescaler-1);//例如prescaler=7200;			//预分频系数为7200-1，这样计数器时钟为72MHz/7200 = 10kHz。注意：预分频系数取值范围为1-65535
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;	//设置时钟分割
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;	//设置计数器模式为向上计数模式
	TIM_TimeBaseStructure.TIM_Period = (period<2)?1:period-1;		//设置计数溢出大小，每计period个数就产生一个更新事件
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseStructure);			//将配置应用到TIM2中
	TIM_ARRPreloadConfig(TIM2, DISABLE);					//禁止ARR预装载缓冲器
}
void Tim2NvicInit(void)
{
	
	NVIC_InitTypeDef NVIC_InitStructure;
	// 配置TIM中断	
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;      	// 选择TIM2的中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	// 抢占式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		// 响应式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			// 中断使能
	NVIC_Init(&NVIC_InitStructure);							// 配置中断
}

/*******************************************************************************************************
** 函数: Tim2Init, Tim定时器延时函数
**------------------------------------------------------------------------------------------------------
** 参数: prescaler 预分频系数
         period  计数周期
Example:
若：
	TIM_Period 即Auto Reload Register(ARR) = 1000  , TIM_Prescaler--71 
则：中断周期为 = 1/(72MHZ /72) * 1000 = 1ms

	TIMxCLK/CK_PSC --> TIMxCNT --> TIM_Period(ARR) --> 中断 且TIMxCNT重置为0重新计数 
        
** 返回: void
********************************************************************************************************/
void Tim2Init(int prescaler,int period)
{
  Tim2ModeInit(prescaler,period);
  Tim2NvicInit();
	

  TIM_ClearFlag(TIM2, TIM_FLAG_Update);					//清除溢出中断标志
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);				//开启TIM2的中断
	TIM_Cmd(TIM2,ENABLE);
	
}


void  TIM2_IRQHandler(void)
{
	if ( TIM_GetITStatus( TIM2, TIM_IT_Update) != RESET ) 
	{	
		//----------------------------------------------------------
		//填充用户代码 Add User Code
#if SPWM_EN ==1	
	  if(bIsPostiveHalf == 0)
		{
			TIM_SetCompare1(TIM1, ((DutyCycleSineWaveTable[SineTablePos]/SamplesAmplitude )*Tim1Period)/MaxDutyCycle);
      TIM_SetCompare2(TIM1, 0);
			SineTablePos ++;
			if(SineTablePos == 128)
			{
				SineTablePos =0;
				bIsPostiveHalf = 1;
			}
		}
		else
		{
			TIM_SetCompare1(TIM1, 0);
      TIM_SetCompare2(TIM1, ((DutyCycleSineWaveTable[SineTablePos]/4096.0 )*Tim1Period)/2.0);
			SineTablePos ++;
			if(SineTablePos == 128)
			{
				SineTablePos =0;
				bIsPostiveHalf = 0;
			}
			
		}
		
#else
		#if USART2_EN == 1
		    UsartSendString(USART2,"Timer2 Update Interrupt occur!\n");
		#endif
#endif
	
		//---------------------------------------------------------
		TIM_ClearITPendingBit(TIM2 , TIM_FLAG_Update);  		 
	}		 	
}


//--------------------------定时器2相关结束-----------------------------------------------//


//--------------------------定时器3相关开始-----------------------------------------------//
//演示PWM功能
static void Tim3GpioInit(void) 
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // 输出比较通道1 GPIO 初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;  //CH1
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// 输出比较通道2 GPIO 初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7;//CH2
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}


///*
// * 注意：TIM_TimeBaseInitTypeDef结构体里面有5个成员，TIM6和TIM7的寄存器里面只有
// * TIM_Prescaler和TIM_Period，所以使用TIM6和TIM7的时候只需初始化这两个成员即可，
// * 另外三个成员是通用定时器和高级定时器才有.
// *-----------------------------------------------------------------------------
// *typedef struct
// *{ TIM_Prescaler            都有
// *	TIM_CounterMode			     TIMx,x[6,7]没有，其他都有
// *  TIM_Period               都有
// *  TIM_ClockDivision        TIMx,x[6,7]没有，其他都有
// *  TIM_RepetitionCounter    TIMx,x[1,8,15,16,17]才有
// *}TIM_TimeBaseInitTypeDef; 
// *-----------------------------------------------------------------------------
// */

/* ----------------   PWM信号 周期和占空比的计算--------------- */
// ARR ：自动重装载寄存器的值
// CLK_cnt：计数器的时钟，等于 Fck_int / (psc+1) = 72M/(psc+1)
// PWM 信号的周期 T = (ARR+1) * (1/CLK_cnt) = (ARR+1)*(PSC+1) / 72M
// 占空比P=CCR/(ARR+1)
//C99 标准编译 请勾选 option for target->c/c++->C99 mode
static void Tim3ModeInit(void)
{
  // 开启定时器时钟,即内部时钟CK_INT=72M
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);
	
  TIM_DeInit(TIM3);										//重新将Timer设置为缺省值
	TIM_InternalClockConfig(TIM3);							//采用内部时钟给TIM3提供时钟源
	
/*--------------------时基结构体初始化-------------------------*/
	// 配置周期，这里配置为10K,根据具体频率更改宏定义GENERAL_TIM3_Period、GENERAL_TIM3_Prescaler值
	
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	// 自动重装载寄存器的值，累计TIM_Period+1个频率后产生一个更新或者中断
	TIM_TimeBaseStructure.TIM_Period=GENERAL_TIM3_Period-1;	
	// 驱动CNT计数器的时钟 = Fck_int/(psc+1)
	TIM_TimeBaseStructure.TIM_Prescaler= GENERAL_TIM3_Prescaler-1;	
	// 时钟分频因子 ，配置死区时间时需要用到
	TIM_TimeBaseStructure.TIM_ClockDivision=TIM_CKD_DIV1;		
	// 计数器计数模式，设置为向上计数
	TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up;		
	// 重复计数器的值，没用到不用管
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;	
	// 初始化定时器
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/*--------------------输出比较结构体初始化-------------------*/	
	// 占空比配置
	uint16_t CCR1_Val = 50;
	uint16_t CCR2_Val = 40;

	TIM_OCInitTypeDef  TIM_OCInitStructure;
	// 配置为PWM模式1
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	// 输出使能
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	// 输出通道电平极性配置	
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;//TIM_OCMode_PWM1模式下，小于CCR值为输出高电平
	
	// 输出比较通道 1
	TIM_OCInitStructure.TIM_Pulse = CCR1_Val;
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	
	// 输出比较通道 2
	TIM_OCInitStructure.TIM_Pulse = CCR2_Val;
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);
	TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);
	
	// 使能计数器
	TIM_Cmd(TIM3, ENABLE);
}

void Tim3Init(void)//Timer3 配置为输出比较模式，产生两个通道的PWM波
{
	Tim3GpioInit();
	Tim3ModeInit();		
}

//--------------------------定时器3相关结束-----------------------------------------------//


//--------------------------定时器4相关开始-----------------------------------------------//
//演示输入捕获功能
void	Tim4GpioInit(void);
void	Tim4ModeInit(void);
void	Tim4NvicInit(void);
	
void Tim4Init(void)//Timer4配置为输入捕获模式，产生1个通道输入捕获，可扩展
{
	Tim4GpioInit();
	Tim4ModeInit();
	Tim4NvicInit();
	
	TIM_ClearFlag(TIM4, TIM_FLAG_Update|TIM_IT_CC1);					//清除溢出中断标志
	TIM_ITConfig(TIM4,TIM_IT_Update|TIM_IT_CC1,ENABLE);				//开启TIM2的中断
	TIM_Cmd(TIM4, ENABLE);
	
}

void	Tim4GpioInit(void)
{
	 GPIO_InitTypeDef GPIO_InitStructure;

  // Timer4输入捕获通道1 GPIO 初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;  //CH1
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
//	GPIO_ResetBitS(GPIOB,GPIO_Pin_6);
}
void	Tim4ModeInit(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_ICInitTypeDef TIM_ICInitStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
	
	TIM_DeInit(TIM4);										//重新将Timer设置为缺省值
	TIM_InternalClockConfig(TIM4);							//采用内部时钟给TIM4提供时钟源
	TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t)(GENERAL_TIM4_Prescaler-1);//例如prescaler=72;			//预分频系数为72-1，这样计数器时钟为72MHz/72 = 1MHz。注意：预分频系数取值范围为1-65535
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;	//设置时钟分割
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;	//设置计数器模式为向上计数模式
	TIM_TimeBaseStructure.TIM_Period = GENERAL_TIM4_Period-1;		//设置计数溢出大小，每计period个数就产生一个更新事件
	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseStructure);			//将配置应用到TIM4中
	TIM_ARRPreloadConfig(TIM4, DISABLE);					//禁止ARR预装载缓冲器
	
	TIM_ICInitStruct.TIM_Channel =  TIM_Channel_1;
  TIM_ICInitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStruct.TIM_ICFilter = 0x00;
	TIM_ICInitStruct.TIM_ICPolarity = TIM_ICPolarity_Rising;
	
	TIM_ICInit(TIM4, & TIM_ICInitStruct);
	
}
void	Tim4NvicInit(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	// 配置TIM中断	
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;      	// 选择TIM4的中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	// 抢占式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		// 响应式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			// 中断使能
	NVIC_Init(&NVIC_InitStructure);							// 配置中断
}


WaveCapture waveCapture = {RESET,0,0,0,0};

void  TIM4_IRQHandler(void)
{
	if(waveCapture.ucFinishFlag == 0)
	{	
		if ( TIM_GetITStatus( TIM4, TIM_IT_Update) != RESET ) 
		{	
			//----------------------------------------------------------
			//填充用户代码 Add User Code
			if(	waveCapture.Egde == SET)
				waveCapture.usUpdateCnt ++;
			//---------------------------------------------------------
			TIM_ClearITPendingBit(TIM4 , TIM_FLAG_Update);  		 
		}	
		
		if ( TIM_GetITStatus( TIM4, TIM_IT_CC1) != RESET ) 
		{	
			//----------------------------------------------------------
			//填充用户代码 Add User Code
			
			if(waveCapture.Egde == RESET)//第一次捕获到上升沿
			{
				
				waveCapture.usCaptureRisingVal = 0;
				waveCapture.usUpdateCnt = 0;
				waveCapture.Egde = SET;
				TIM_SetCounter(TIM4, 0);
			}
			else
			{
				
				waveCapture.usCaptureRisingVal = TIM_GetCapture1(TIM4);
				waveCapture.ulFrequency = 1000000/(waveCapture.usUpdateCnt * 65536 +waveCapture.usCaptureRisingVal );
				waveCapture.usCaptureRisingVal = 0;
				waveCapture.usUpdateCnt = 0;
				waveCapture.Egde = RESET;
				waveCapture.ucFinishFlag  = 1;
			}
			//---------------------------------------------------------
			TIM_ClearITPendingBit(TIM4 , TIM_IT_CC1);  		 
		}
	}
	else
		TIM_ClearITPendingBit(TIM4 , TIM_IT_Update|TIM_IT_CC1);  
  	
}


//--------------------------定时器4相关结束-----------------------------------------------//



//--------------------------高级定时器1相关开始-----------------------------------------------//

void	Tim1GpioInit(void);
void	Tim1ModeInit(void);

void 	Tim1Init(void)
{
	Tim1GpioInit();
	Tim1ModeInit();
}

void	Tim1GpioInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;	

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE | RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_FullRemap_TIM1,ENABLE);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	
	GPIO_ResetBits(GPIOE,GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11);
}

void	Tim1ModeInit(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	TIM_BDTRInitTypeDef TIM_BDTRInitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 , ENABLE);
	
  TIM_DeInit(TIM1);
   /* TIM1 Peripheral Configuration ----------------------------------------*/
  /* Time Base configuration */
#if SPWM_EN == 1
	TIM_TimeBaseStructure.TIM_Prescaler =(uint16) (Tim1Prescaler-1);
  TIM_TimeBaseStructure.TIM_Period = (uint16)(Tim1Period -1) ;
#else
  TIM_TimeBaseStructure.TIM_Prescaler =(uint16) (Advance_TIM1_Prescaler -1);
  TIM_TimeBaseStructure.TIM_Period = (uint16)(Advance_TIM1_Period -1) ;
#endif	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;

  TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);

  /* Channel 1 Configuration in PWM mode */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
  TIM_OCInitStructure.TIM_Pulse = Advance_TIM1_Period * 0.3;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;//TIM_OCMode_PWM1模式下，小于CCR值为输出高电平
  TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
  TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
  TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCIdleState_Reset;

  TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC2Init(TIM1, &TIM_OCInitStructure);
	
	 /* Automatic Output enable, Break, dead time and lock configuration*/
  TIM_BDTRInitStructure.TIM_OSSRState = TIM_OSSRState_Enable;
  TIM_BDTRInitStructure.TIM_OSSIState = TIM_OSSIState_Enable;
  TIM_BDTRInitStructure.TIM_LOCKLevel = TIM_LOCKLevel_1;
  TIM_BDTRInitStructure.TIM_DeadTime = 30;
  TIM_BDTRInitStructure.TIM_Break = TIM_Break_Disable;
  TIM_BDTRInitStructure.TIM_BreakPolarity = TIM_BreakPolarity_High;
  TIM_BDTRInitStructure.TIM_AutomaticOutput = TIM_AutomaticOutput_Disable;//TIM_AutomaticOutput_Enable;//

  TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);
	
	
  TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);//CCR值更改时，等待计数完一个周期时才更新相应影子寄存器
  TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Enable);
	
	TIM_ARRPreloadConfig(TIM1, ENABLE);//ARR值更改时，等待计数完一个周期时才更新相应影子寄存器
	
	TIM_Cmd(TIM1, ENABLE);
	
  /* Main Output Enable */
  TIM_CtrlPWMOutputs(TIM1, ENABLE);
	
}


//--------------------------SPWM波相关开始----------------------------------------//


#if SPWM_EN == 1
	
void SpwmInit(void)
{
	Tim2Init(Tim2Prescaler,Tim2Period);
	Tim1Init();
	
	
}

#endif



//--------------------------SPWM波相关结束------------------------------------ ---//

//--------------------------高级定时器1相关结束-----------------------------------------------//




