/*******************************************************************************
 * @file    bsp_led.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_LED_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_led.h"

#if LED_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/


uint8_t LED_ToggleEnable = 1;


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Led_Init(void)
{
 	GPIO_InitTypeDef  GPIO_InitStruct;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin  = GPIO_Pin_2 ;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStruct.GPIO_Pin  = GPIO_Pin_3 ;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

//    Task_Append(TASK_ID_LED, Led_Toggle, 250); //添加LED闪烁任务，回调函数led_toggle，周期250ms
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Led_Task_Handler(void)
{
	GPIOC->ODR ^= GPIO_Pin_2;
#if USART3_ENABLE
	Usart3_SendString((unsigned char*)"led(PC.2) Toggle (usart3)\r\n");
#endif
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Led_Shell_Handler(uint8_t OnOff)
{
	if(OnOff == 1)
		GPIO_ResetBits(GPIOC,GPIO_Pin_3);
	else
		GPIO_SetBits(GPIOC,GPIO_Pin_3);
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

