/*******************************************************************************
 * @file    bsp_usart1.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_USART1_H__
#define __BSP_USART1_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_USART1_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if USART1_ENABLE

/* Exported constants --------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/



/* Exported macro ------------------------------------------------------------*/
#define USART1_BAUD         115200
#define USART1_RX_BUF_SIZE  256

/* Exported variables --------------------------------------------------------*/
EXTERN uint8_t   	g_USART1_RxBuf[USART1_RX_BUF_SIZE];//串口接收缓冲区
EXTERN uint16_t 	g_USART1_RecPos;//存放当前串口接收数据存放的位置
EXTERN struct rt_ringbuffer usart1_recv_ring_buf;//串口1接收环形队列
/* Exported functions --------------------------------------------------------*/

EXTERN void Usart1_Init(void);
EXTERN void Usart1_SendString(uint8_t *str);//发送字符串
EXTERN void Usart1Recv_Task_Handler(void);
#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

