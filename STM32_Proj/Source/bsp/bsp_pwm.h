/*******************************************************************************
 * @file    bsp_pwm.h
 * @author  Harm
 * @version V1.00
 * @date    17-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_PWM_H__
#define __BSP_PWM_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_PWM_C__
#define EXTERN
#else
#define EXTERN extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if PWM_ENABLE

/* Exported constants --------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define GENERAL_TIM3_Period    100
#define GENERAL_TIM3_Prescaler  72

/* Exported functions --------------------------------------------------------*/



EXTERN void PWM_Init(void);
EXTERN void shell_PWM_Test_cmd(char argc, char *argv);
#endif


#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

