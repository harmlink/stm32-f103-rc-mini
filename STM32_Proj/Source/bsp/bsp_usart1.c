/*******************************************************************************
 * @file    bsp_usart1.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_USART1_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_usart1.h"
#include "nr_micro_shell.h"

#if USART1_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
uint8_t   g_USART1_RxBuf[USART1_RX_BUF_SIZE];//串口接收缓冲区
uint16_t  g_USART1_RecPos = 0;//存放当前串口接收数据存放的位置
/* Exported function prototypes ----------------------------------------------*/
struct rt_ringbuffer usart1_recv_ring_buf;//串口1接收环形缓冲队列
uint8_t     s_USART1_RxBuf[USART1_RX_BUF_SIZE];//串口接收缓冲区

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Usart1_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef  USART_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_9 ;  //TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_10 ;  //RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	USART_InitStructure.USART_BaudRate = USART1_BAUD;  
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity =  USART_Parity_No;
	USART_InitStructure.USART_Mode = USART_Mode_Rx |USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART1, &USART_InitStructure);//初始化串口1
	
	//串口1中断初始化
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART1, ENABLE);//使能串口1
	
	USART_ClearFlag(USART1, USART_FLAG_TC  );//清发送完成标志位

	rt_ringbuffer_init(&usart1_recv_ring_buf,s_USART1_RxBuf,USART1_RX_BUF_SIZE);
	shell_init();//人机交互nr_micro_shell初始化
}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
static void Usart1_SendByte(uint8_t ch)
{
	
	USART_SendData(USART1, (uint8_t)ch);
	
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC)== RESET);
}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Usart1_SendString(uint8_t *str) //发送字符串
{
	uint32_t pos = 0;
	while(*(str+pos)!='\0')
	{
		Usart1_SendByte(*(str+pos));
		pos ++;
		
	}
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//为能够调用printf函数从选定的串口打印输出，重定义fputc函数，本例子使用串口1
int fputc(int ch,FILE *f)
{
	
	USART_SendData(USART1, (uint8_t)ch);
	
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC)== RESET);
	
	return (ch);

}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//USART1_IRQHandler,串口1中断回调函数
void USART1_IRQHandler(void)
{

	uint8_t RecCh;

	if( USART_GetFlagStatus(USART1,USART_FLAG_RXNE)!=RESET )				// 串口接收数据 
	{		
        
		Usart1RecTimer = 10;
		RecCh =(uint8)USART_ReceiveData(USART1);
		g_USART1_RxBuf[g_USART1_RecPos++] =RecCh;
        
		rt_ringbuffer_put(&usart1_recv_ring_buf,&RecCh,1);
		//shell(RecCh);//人机交互nr_micro_shell
		
		USART_ClearFlag(USART1, USART_FLAG_RXNE);
	}
	if( USART_GetFlagStatus(USART1,USART_FLAG_ORE)==SET ) 				// 串口溢出错误
	{
		USART_ClearFlag(USART1, USART_FLAG_ORE);
	}
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
unsigned char Usart1_RecProcess(void)
{
	
	if(Usart1RecTimer) return FALSE;
	if(!g_USART1_RecPos) return FALSE;
	
	g_USART1_RxBuf[g_USART1_RecPos]='\0';
	
	//收包处理
//	"Beep On" 
//	"BeeP Off"
	if(strstr((char *)g_USART1_RxBuf,"Beep On")!= NULL)
	{
		//BeepOn();
	}
	else if(strstr((char *)g_USART1_RxBuf,"Beep Off")!= NULL)
	{
		//BeepOff();
	}
	//memset(g_USART1_RxBuf,0,USART1_RX_BUF_SIZE);
	g_USART1_RecPos = 0;
	


	return TRUE;
}


void Usart1Recv_Task_Handler(void)
{
	static uint8_t RecCh = 0 ;
	
	if(rt_ringbuffer_data_len(&usart1_recv_ring_buf) != 0) //串口1不为空，则需要处理
	{
		rt_ringbuffer_get(&usart1_recv_ring_buf, &RecCh, 1);
		shell(RecCh);
		//以下解决普通串口调试助手(ATK_COM、有人串口等)问题
		while(((RecCh == '\r') || (RecCh == '\n')))
		{
			if(rt_ringbuffer_data_len(&usart1_recv_ring_buf) != 0)
			{
			  rt_ringbuffer_get(&usart1_recv_ring_buf, &RecCh, 1);
			  if((RecCh != '\r') && (RecCh != '\n')) shell(RecCh);
			}
		    else
				break;
		}
		
		

	}
}
#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

