/*******************************************************************************
 * @file    bsp_dac.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_DAC_H__
#define __BSP_DAC_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_DAC_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if DAC_ENABLE

/* Exported constants --------------------------------------------------------*/



/* Exported types ------------------------------------------------------------*/



/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
EXTERN void Dac_Init(void);
EXTERN void Dac_SetVol(float vol);
EXTERN void Dac_Task_Handler(void);

#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

