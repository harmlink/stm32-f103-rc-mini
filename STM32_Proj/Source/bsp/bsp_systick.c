/*******************************************************************************
 * @file    bsp_systick.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_SYSTICK_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_SYSTICK.h"



/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
static uint32_t g_iRunTimer = 0;//定义系统全局运行时间
volatile uint32_t OS_Tick = 0; //仿操作系统时间滴答变量，每隔1ms增加1
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
uint32_t sysTimer[MAX_SOFT_TIMER];//软件定时器数组


/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void SysTick_Init(void)
{
	uchar i;
	for(i = 0;i < MAX_SOFT_TIMER; i++)
		sysTimer[i] = 0;

	SysTick_Config(72000000/1000);//每隔1ms进一次系统滴答中断响应函数，主频72M
	
}



/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void SysTick_Handler(void) 
{
	uint8 i;
	for(i = 0;i < MAX_SOFT_TIMER; i++)
	  if(sysTimer[i])
			sysTimer[i]--;
		
		
	if(g_iRunTimer++ == 0XFFFFFFFF)//最多可计50天左右
		 g_iRunTimer = 0;
	
	
	OS_Tick++;
	Task_TimeSlice(OS_Tick);//任务时间片处理
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void SysTick_DelayMs(uint32 nms)
{
	CPU_INT_ENABLE();//打开CPU中断
	
	DelayTimer = nms;
	while(DelayTimer);//等待延时时间完成
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void SysTick_DelayUs(uint32 nus) //注意该函数延时不能超过1ms 
{
    uint32 delta;

    /* 获得延时经过的tick数 */
    nus = nus * (SysTick->LOAD/(1000000/1000));

    /* 获得当前时间 */
    delta = SysTick->VAL;

    /* 循环获得当前时间，直到达到指定的时间后退出循环 */
    while (delta - SysTick->VAL< nus);
}


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

