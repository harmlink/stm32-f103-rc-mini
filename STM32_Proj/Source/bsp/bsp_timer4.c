/*******************************************************************************
 * @file    bsp_timer4.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_TIMER4_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_timer4.h"

#if TIMER4_CAPTURE_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/



/* Private function prototypes -----------------------------------------------*/
static void	Tim4GpioInit(void);
static void	Tim4ModeInit(void);
static void	Tim4NvicInit(void);
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/

//演示输入捕获功能

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
	
void Tim4Init(void)//Timer4配置为输入捕获模式，产生1个通道输入捕获，可扩展
{
	Tim4GpioInit();
	Tim4ModeInit();
	Tim4NvicInit();
	
	TIM_ClearFlag(TIM4, TIM_FLAG_Update|TIM_IT_CC1);					//清除溢出中断标志
	TIM_ITConfig(TIM4,TIM_IT_Update|TIM_IT_CC1,ENABLE);				//开启TIM2的中断
	TIM_Cmd(TIM4, ENABLE);
	
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

void Tim4GpioInit(void)
{
	 GPIO_InitTypeDef GPIO_InitStructure;

  // Timer4输入捕获通道1 GPIO 初始化
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6;  //CH1
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
//	GPIO_ResetBitS(GPIOB,GPIO_Pin_6);
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

void Tim4ModeInit(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_ICInitTypeDef TIM_ICInitStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4,ENABLE);
	
	TIM_DeInit(TIM4);										//重新将Timer设置为缺省值
	TIM_InternalClockConfig(TIM4);							//采用内部时钟给TIM4提供时钟源
	TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t)(GENERAL_TIM4_Prescaler-1);//例如prescaler=72;			//预分频系数为72-1，这样计数器时钟为72MHz/72 = 1MHz。注意：预分频系数取值范围为1-65535
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;	//设置时钟分割
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;	//设置计数器模式为向上计数模式
	TIM_TimeBaseStructure.TIM_Period = GENERAL_TIM4_Period-1;		//设置计数溢出大小，每计period个数就产生一个更新事件
	TIM_TimeBaseInit(TIM4,&TIM_TimeBaseStructure);			//将配置应用到TIM4中
	TIM_ARRPreloadConfig(TIM4, DISABLE);					//禁止ARR预装载缓冲器
	
	TIM_ICInitStruct.TIM_Channel =  TIM_Channel_1;
	TIM_ICInitStruct.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStruct.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStruct.TIM_ICFilter = 0x00;
	TIM_ICInitStruct.TIM_ICPolarity = TIM_ICPolarity_Rising;
	
	TIM_ICInit(TIM4, & TIM_ICInitStruct);
	
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

void Tim4NvicInit(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	// 配置TIM中断	
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;      	// 选择TIM4的中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	// 抢占式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		// 响应式中断优先级设置为0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			// 中断使能
	NVIC_Init(&NVIC_InitStructure);							// 配置中断
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/

WaveCapture waveCapture = {RESET,0,0,0,0};

void  TIM4_IRQHandler(void)
{
	if(waveCapture.ucFinishFlag == 0)
	{	
		if ( TIM_GetITStatus( TIM4, TIM_IT_Update) != RESET ) 
		{	
			//----------------------------------------------------------
			//填充用户代码 Add User Code
			if(	waveCapture.Egde == SET)
				waveCapture.usUpdateCnt ++;
			//---------------------------------------------------------
			TIM_ClearITPendingBit(TIM4 , TIM_FLAG_Update);  		 
		}	
		
		if ( TIM_GetITStatus( TIM4, TIM_IT_CC1) != RESET ) 
		{	
			//----------------------------------------------------------
			//填充用户代码 Add User Code
			
			if(waveCapture.Egde == RESET)//第一次捕获到上升沿
			{
				
				waveCapture.usCaptureRisingVal = 0;
				waveCapture.usUpdateCnt = 0;
				waveCapture.Egde = SET;
				TIM_SetCounter(TIM4, 0);
			}
			else
			{
				
				waveCapture.usCaptureRisingVal = TIM_GetCapture1(TIM4);
				waveCapture.ulFrequency = 1000000/(waveCapture.usUpdateCnt * 65536 +waveCapture.usCaptureRisingVal );
				waveCapture.usCaptureRisingVal = 0;
				waveCapture.usUpdateCnt = 0;
				waveCapture.Egde = RESET;
				waveCapture.ucFinishFlag  = 1;
			}
			//---------------------------------------------------------
			TIM_ClearITPendingBit(TIM4 , TIM_IT_CC1);  		 
		}
	}
	else
		TIM_ClearITPendingBit(TIM4 , TIM_IT_Update|TIM_IT_CC1);  
  	
}

void Timer4_Capture_Task_Handler(void)
{
	if(waveCapture.ucFinishFlag == 1)
	{
		printf("Frequency = %d Hz\r\n",waveCapture.ulFrequency);
		waveCapture.ucFinishFlag = 0;
	}
	else
	{   //通道无输入脉冲，则无输出
		//printf("Wave is not exist or Cature is incomplete.\n");
	}
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

