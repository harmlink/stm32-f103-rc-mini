/*******************************************************************************
 * @file    bsp_inflash.h
 * @author  Harm
 * @version V1.00
 * @date    21-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_INFLASH_H__
#define __BSP_INFLASH_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_INFLASH_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if INFLASH_ENABLE

/* Exported constants --------------------------------------------------------*/



/* Exported types ------------------------------------------------------------*/




/* Exported macro ------------------------------------------------------------*/

#define STM32_FLASH_SIZE  256                   //256K Flash ,单位为K
#if STM32_FLASH_SIZE < 256
	#define FLASH_PAGE_SIZE ((uint16_t)0x400)   //页面大小为1K
#else
	#define FLASH_PAGE_SIZE ((uint16_t)0x800)     //页面大小为2K
#endif

//FLASH地址与读写相关的宏定义
#define USER_ALLOW_FLASH_REGIO_SIZE  	32 //单位K
#define CODE_SIZE 					((STM32_FLASH_SIZE-USER_ALLOW_FLASH_REGIO_SIZE)*0x400) //0x400 代表 1k,总共代码区设为(STM32_FLASH_SIZE-32)k,根据实际代码大小设置
#define START_ADDR 					(0x08000000) //Flash起始地址
#define FLASH_USER_ADDR 			(START_ADDR+CODE_SIZE) //可供用户使用的Flash起始地址

#define StmInFlash_ReadBytes(Addr,Buff,Size)  	InFlashReadBytes(FLASH_USER_ADDR+Addr,Buff,Size) //读数据
#define StmInFlash_WriteBytes(Addr,Buff,Size)  	InFlashWriteBytes(FLASH_USER_ADDR+Addr,Buff,Size) //写数据


/* Exported functions --------------------------------------------------------*/

EXTERN int InFlashReadBytes(uint32_t Addr,uint8_t *pBuf,uint32_t size);
EXTERN int InFlashWriteBytes(uint32_t Addr,uint8_t *pBuf,uint32_t size);
EXTERN void shell_Inflash_Write_cmd(char argc, char *argv);
EXTERN void shell_Inflash_Read_cmd(char argc, char *argv);

#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

