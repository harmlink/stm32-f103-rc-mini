/*******************************************************************************
 * @file    bsp_beep.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_BEEP_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_beep.h"

#if BEEP_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/





/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Beep_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	BEEP_APBxClock_FUN(BEEP_APBxClock_CLK, ENABLE);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Pin  = BEEP_GPIO_PIN ;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(BEEP_GPIO_PORT, &GPIO_InitStruct);
	
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Beep_Control(uint8_t OnOff)
{
	if(OnOff == BEEP_ON_LEVEL)
		BEEP_ON(); //GPIO_SetBits(GPIOA,GPIO_Pin_5);
	else
		BEEP_OFF();//GPIO_ResetBits(GPIOA,GPIO_Pin_5);
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
uint8_t beep_enable = 0;
void Beep_Task_Handler(void)
{
	static uint8_t cnt=0;
	if(beep_enable == 1)
	{
		if( cnt == 0)
		{
			BEEP_ON();
			cnt++;
			cnt = cnt%2;
		}
		else
		{
			BEEP_OFF();
			cnt++;
			cnt = cnt%2;
		}
	}
	else
		BEEP_OFF();
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
/**
 * @brief beep command
 */
void shell_beep_cmd(char argc, char *argv)
{
    char *param = &(argv[argv[1]]);

    if(strcmp(param, "1") == 0)
    {
		beep_enable = 1;
    }
    else if(strcmp(param, "0") == 0)
    {
		beep_enable = 0;
    }
	else
	{
		printf("Error Command! Usage:beep 1 or beep 0\r\n");
	}
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

