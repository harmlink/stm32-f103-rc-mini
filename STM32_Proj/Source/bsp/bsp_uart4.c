/*******************************************************************************
 * @file    bsp_uart4.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_UART4_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_uart4.h"


#if UART4_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
uint8_t   g_UART4_RxBuf[UART4_RX_BUF_SIZE];//串口接收缓冲区
uint16_t  g_UART4_RecPos = 0;//存放当前串口接收数据存放的位置
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Uart4_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef  USART_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);//注意uart4为AP1总线外设

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_10 ;  //TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_11 ;  //RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	
	USART_InitStructure.USART_BaudRate = UART4_BAUD;  
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity =  USART_Parity_No;
	USART_InitStructure.USART_Mode = USART_Mode_Rx |USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(UART4, &USART_InitStructure);//初始化串口4
	
	//串口4中断初始化
	NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);

	USART_Cmd(UART4, ENABLE);//使能串口4
	
	USART_ClearFlag(UART4, USART_FLAG_TC);//清发送完成标志位


}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
static void Uart4_SendByte(uint8_t ch)
{
	
	USART_SendData(UART4, (uint8_t)ch);
	
	while(USART_GetFlagStatus(UART4, USART_FLAG_TC)== RESET);
}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Uart4_SendString(uint8_t *str) //发送字符串
{
	uint32_t pos = 0;
	while(*(str+pos)!='\0')
	{
		Uart4_SendByte(*(str+pos));
		pos ++;
		
	}
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//UART4_IRQHandler,串口4中断回调函数
void UART4_IRQHandler(void)
{

	uint8_t RecCh;

	if( USART_GetFlagStatus(UART4,USART_FLAG_RXNE)!=RESET )				// 串口接收数据 
	{		
        
		Uart4RecTimer = 10;
		RecCh =(uint8)USART_ReceiveData(UART4);
		g_UART4_RxBuf[g_UART4_RecPos++] =RecCh;
        
		USART_ClearFlag(UART4, USART_FLAG_RXNE);
	}
	if( USART_GetFlagStatus(UART4,USART_FLAG_ORE)==SET ) 				// 串口溢出错误
	{
		USART_ClearFlag(UART4, USART_FLAG_ORE);
	}
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
unsigned char Uart4_RecProcess(void)
{
	
	if(Uart4RecTimer) return FALSE;
	if(!g_UART4_RecPos) return FALSE;
	
	g_UART4_RxBuf[g_UART4_RecPos]='\0';
	
/*********************收包处理  Begin***************/
//	"Beep On" 
//	"BeeP Off"
	if(strstr((char *)g_UART4_RxBuf,"Beep On")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_ON();
		#endif
		Uart4_SendString((unsigned char*)"uart4 recv\r\n");
	}
	else if(strstr((char *)g_UART4_RxBuf,"Beep Off")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_OFF();
		#endif
		Uart4_SendString((unsigned char*)"uart4 recv\r\n");
	}
/*********************收包处理  End ***************/
	//memset(g_UART4_RxBuf,0,UART4_RX_BUF_SIZE);
	g_UART4_RecPos = 0;
	
	return TRUE;
}

void Uart4Recv_Task_Handler(void)
{
	Uart4_RecProcess();
}

#endif


//#include <stdarg.h>

//void u4_printf(char *fmt, ...) {
//    int i;
//    int len;
//    char buffer[50]; //足够容纳才可以，可以搞大点
//    va_list args;
//    va_start(args, fmt);
//    vsprintf(buffer, fmt, args);
//    va_end(args);

//    len = strlen(buffer);

//    for (i = 0; i < len; i++) {
//        while (USART_GetFlagStatus(UART4, USART_FLAG_TXE) == RESET); //等待发送完毕
//        USART_SendData(UART4, buffer[i]); //发送一个字符
//    }
//}

/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

