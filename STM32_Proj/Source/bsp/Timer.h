#ifndef __TIMER__H__
#define __TIMER__H__

#include "stm32f10x.h"

#define uint32 unsigned int
#define uint16 unsigned short int
#define uint8 unsigned char

//----------------------定时器2相关定义------------------------------------------------//
//演示定时功能
void Tim2Init(int prescaler,int period);

//----------------------定时器2相关结束------------------------------------------------//


//----------------------定时器3相关结束------------------------------------------------//
//演示PWM功能
#define GENERAL_TIM3_Period    100
#define GENERAL_TIM3_Prescaler  72
void Tim3Init(void);//Timer3 配置为输出比较模式，产生两个通道的PWM波
//----------------------定时器3相关结束------------------------------------------------//


//----------------------定时器4相关开始------------------------------------------------//
//演示输入捕获功能
typedef struct
{
	FlagStatus Egde;//Egde = SET表示当前处于高电平;Egde = RESET表示当前处于低电平;
	uint8 ucFinishFlag;//捕获结束标记
	uint16 usCaptureRisingVal;//输入捕获值
	uint16 usUpdateCnt;//计数溢出次数
	uint32 ulFrequency;//输入波形的频率
	
}WaveCapture;

extern WaveCapture waveCapture;
#define GENERAL_TIM4_Period    0xffff
#define GENERAL_TIM4_Prescaler  72
void Tim4Init(void);//Timer4配置为输入捕获模式，产生1个通道输入捕获，可扩展
//----------------------定时器4相关结束------------------------------------------------//


//--------------------------高级定时器1相关开始----------------------------------------//
#define Advance_TIM1_Period    (72000000/10000) //注意此值不能超过65536
#define Advance_TIM1_Prescaler  1
void Tim1Init(void);

//--------------------------高级定时器1相关结束------------------------------------ ---//

//--------------------------SPWM波相关开始----------------------------------------//
#define SPWM_EN 0

#if SPWM_EN == 1
	#define FundamentalFrequency 50 //SPWM波基波频率，单位HZ
  #define SamplesPerHalfCycle 128  //半周期占空比个数
	#define SamplesAmplitude 4096.0  //SPWM占空比最大值
	
  #define  CarrierFrequency 20000 //SPWM波载波频率，单位HZ
	#define  Tim1Period (72000000/(CarrierFrequency)) //此值不能超过65536
	#define  Tim1Prescaler  1
	
	#define  MaxDutyCycle 0.5

	#define Tim2Period (72000000/(SamplesPerHalfCycle*2*FundamentalFrequency))
	#define Tim2Prescaler 1

  void SpwmInit(void);

#endif



//--------------------------SPWM波相关结束------------------------------------ ---//

#endif

