/*******************************************************************************
 * @file    bsp_at24c02.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_AT24C02_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_at24c02.h"


#if  AT24C02_ENABLE

void At24c02Init(void)
{
	I2cGpioInit();
}


static void At24c02WriteByte(unsigned char addr,unsigned char ByteData)
{
	I2cStart();
	
	I2cWriteByte(EEPROM_DEV_ADDR|EEPROM_WR);
	I2cReadAck();
	
	I2cWriteByte(addr);
	I2cReadAck();
	
	I2cWriteByte(ByteData);
	I2cReadAck();
	
	I2cStop();
	
	
	
	
}
	


void At24c02Write(unsigned char addr,unsigned char *buff,int size)
{
	
	int cnt;
	
	for(cnt = 0;cnt < size;cnt++)
	    At24c02WriteByte(addr + cnt,*(buff + cnt));
	
}
void At24c02Read(unsigned char addr,unsigned char *buff,int size)
{
	I2cStart();
	
	I2cWriteByte(EEPROM_DEV_ADDR|EEPROM_WR);
	I2cReadAck();
	
	I2cWriteByte(addr);
	I2cReadAck();
	
	I2cStart();
	
	I2cWriteByte(EEPROM_DEV_ADDR|EEPROM_RD);
	I2cReadAck();
	
	
	while(size--)
	{
		
		I2cReadByte(buff++);
		
		if(size)
			I2cAck();
		else
			I2cNoAck();
		
	}
	
	I2cStop();
	
	
}

/**************AT24C02测试例程****************************/
//如果不需要，可以删除该函数实现及AT24C02.h中的函数声明
char psFont[64]="AT24C02 Test [hello world!,1,2,3,4,5,6,7,8,9,0]";
//char psFont[64]="物理与能源学院";

void AT24C02Test(void)  
{

		char sBuff[64];

		At24c02Write(50,(uint8 *)psFont,strlen(psFont));
		printf("\r\nwrite:%s\r\n",psFont);	
		At24c02Read(50,(uint8 *)sBuff,strlen(psFont));
		printf("\r\n read:%s\r\n",sBuff);	
		

}
/**************AT24C02测试例程****************************/


#endif
