/*******************************************************************************
 * @file    bsp_uart5.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_UART5_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_uart5.h"


#if UART5_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
uint8_t   g_UART5_RxBuf[UART5_RX_BUF_SIZE];//串口接收缓冲区
uint16_t  g_UART5_RecPos = 0;//存放当前串口接收数据存放的位置
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Uart5_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef  USART_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);//注意uart5为AP1总线外设
    //USART_DeInit(UART5);  //复位串口5
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_12 ;  //TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_2 ;  //RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	
	USART_InitStructure.USART_BaudRate = UART5_BAUD;  
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity =  USART_Parity_No;
	USART_InitStructure.USART_Mode = USART_Mode_Rx |USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(UART5, &USART_InitStructure);//初始化串口5
	
	//串口5中断初始化
	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(UART5, USART_IT_RXNE, ENABLE);

	USART_Cmd(UART5, ENABLE);//使能串口5
	
	USART_ClearFlag(UART5, USART_FLAG_TC);//清发送完成标志位


}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
static void Uart5_SendByte(uint8_t ch)
{
	
	USART_SendData(UART5, (uint8_t)ch);
	
	while(USART_GetFlagStatus(UART5, USART_FLAG_TC)== RESET);
}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Uart5_SendString(uint8_t *str) //发送字符串
{
	uint32_t pos = 0;
	while(*(str+pos)!='\0')
	{
		Uart5_SendByte(*(str+pos));
		pos ++;
		
	}
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//UART5_IRQHandler,串口5中断回调函数
void UART5_IRQHandler(void)
{

	uint8_t RecCh;

	if( USART_GetFlagStatus(UART5,USART_FLAG_RXNE)!=RESET )				// 串口接收数据 
	{		
        
		Uart5RecTimer = 10;
		RecCh =(uint8)USART_ReceiveData(UART5);
		g_UART5_RxBuf[g_UART5_RecPos++] =RecCh;
        
		USART_ClearFlag(UART5, USART_FLAG_RXNE);
	}
	if( USART_GetFlagStatus(UART5,USART_FLAG_ORE)==SET ) 				// 串口溢出错误
	{
		USART_ClearFlag(UART5, USART_FLAG_ORE);
	}
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
unsigned char Uart5_RecProcess(void)
{
	
	if(Uart5RecTimer) return FALSE;
	if(!g_UART5_RecPos) return FALSE;
	
	g_UART5_RxBuf[g_UART5_RecPos]='\0';
	
/*********************收包处理  Begin***************/
//	"Beep On" 
//	"BeeP Off"
	if(strstr((char *)g_UART5_RxBuf,"Beep On")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_ON();
		#endif
		Uart5_SendString((unsigned char*)"uart5 recv\r\n");
	}
	else if(strstr((char *)g_UART5_RxBuf,"Beep Off")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_OFF();
		#endif
		Uart5_SendString((unsigned char*)"uart5 recv\r\n");
	}
/*********************收包处理  End ***************/
	//memset(g_UART5_RxBuf,0,UART5_RX_BUF_SIZE);
	g_UART5_RecPos = 0;
	
	return TRUE;
}

void Uart5Recv_Task_Handler(void)
{
	Uart5_RecProcess();
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

