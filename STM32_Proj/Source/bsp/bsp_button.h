/*******************************************************************************
 * @file    bsp_button.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_BUTTON_H__
#define __BSP_BUTTON_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BUTTON_C__
#define EXTERN
#else
#define EXTERN extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if BUTTON_ENABLE

/* Exported constants --------------------------------------------------------*/
#define BUTTON_NUMBER  3


#define BUTTON1_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define BUTTON1_APBxClock_CLK    	RCC_APB2Periph_GPIOA
#define BUTTON1_PORT   				GPIOA
#define BUTTON1_PIN    				GPIO_Pin_0      /* KEY1 */
#define BUTTON1_DOWN_LEVEL      	1      			/* 根据原理图设计，KEY1按下时引脚为高电平，所以这里设置为1 */

#define BUTTON2_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define BUTTON2_APBxClock_CLK    	RCC_APB2Periph_GPIOC
#define BUTTON2_PORT            	GPIOC
#define BUTTON2_PIN             	GPIO_Pin_13   	/* KEY2 */
#define BUTTON2_DOWN_LEVEL      	1      			/* 根据原理图设计，KEY2按下时引脚为高电平，所以这里设置为1 */


/* Exported types ------------------------------------------------------------*/



/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
EXTERN void Button_Init(void);


#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/
