#include "bsp_w25qxx.h"

void w25qxx_spix_init(void)
{
	spi_init(SPI1);
}

#if 0
#define SFUD_DEMO_TEST_BUFFER_SIZE                     1024
static uint8_t sfud_demo_test_buf[SFUD_DEMO_TEST_BUFFER_SIZE];

static void sfud_demo(uint32_t addr, size_t size, uint8_t *data) {
    sfud_err result = SFUD_SUCCESS;
    const sfud_flash *flash = sfud_get_device_table() + 0;
    size_t i;
    /* prepare write data */
    for (i = 0; i < size; i++) {
        data[i] = i;
    }
    /* erase test */
    result = sfud_erase(flash, addr, size);
    if (result == SFUD_SUCCESS) {
        printf("Erase the %s flash data finish. Start from 0x%08X, size is %ld.\r\n", flash->name, addr,
                size);
    } else {
        printf("Erase the %s flash data failed.\r\n", flash->name);
        return;
    }
    /* write test */
    result = sfud_write(flash, addr, size, data);
    if (result == SFUD_SUCCESS) {
        printf("Write the %s flash data finish. Start from 0x%08X, size is %ld.\r\n", flash->name, addr,
                size);
    } else {
        printf("Write the %s flash data failed.\r\n", flash->name);
        return;
    }
    /* read test */
    result = sfud_read(flash, addr, size, data);
    if (result == SFUD_SUCCESS) {
        printf("Read the %s flash data success. Start from 0x%08X, size is %ld. The data is:\r\n", flash->name, addr,
                size);
        printf("Offset (h) 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F\r\n");
        for (i = 0; i < size; i++) {
            if (i % 16 == 0) {
                printf("[%08X] ", addr + i);
            }
            printf("%02X ", data[i]);
            if (((i + 1) % 16 == 0) || i == size - 1) {
                printf("\r\n");
            }
        }
        printf("\r\n");
    } else {
        printf("Read the %s flash data failed.\r\n", flash->name);
    }
    /* data check */
    for (i = 0; i < size; i++) {
        if (data[i] != i % 256) {
            printf("Read and check write data has an error. Write the %s flash data failed.\r\n", flash->name);
			break;
        }
    }
    if (i == size) {
        printf("The %s flash test is success.\r\n", flash->name);
    }
}

#endif
/*****************************w25qxx example****************************************/
char w25qxx_test_string[6][128]={
	"***************************Copyright (c)***************************\r\n",
	"**----------------------cpe.fjnu.edu.cn -------------------------**\r\n",
	"** Thanks to armink:https://gitee.com/Armink/SFUD               **\r\n",
	"** file: bsp_w25qxx.c                                            **\r\n",
	"** description: w25q64.c  example                                **\r\n",
	"*******************************************************************\r\n"
};
void w25qxx_test(void)
{
#if 1
	int i;
	char sBuff[256]={0};
//	sfud_err result = SFUD_SUCCESS;
    const sfud_flash *flash = sfud_get_device_table() + 0;
	printf("W25Q64(SFUD) Test(write then read) begin:\r\n");
	for(i=0;i<6;i++)
	{
		sfud_erase_write(flash, 0, strlen(w25qxx_test_string[i]),(uint8_t *)w25qxx_test_string[i]);
	    sfud_read(flash, 0, strlen(w25qxx_test_string[i]),(uint8_t *) sBuff);
	    printf("%s\n",sBuff);	
	}
	printf("W25Q64(SFUD) Test(write then read) end��\r\n");
#endif
	
#if 0
	 sfud_demo(0, sizeof(sfud_demo_test_buf), sfud_demo_test_buf);
#endif
	
}
/*********************************END OF FILE****************************************/
