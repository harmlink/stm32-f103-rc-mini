/*******************************************************************************
 * @file    bsp_button.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BUTTON_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_button.h"
#include "multi_button.h"

#if BUTTON_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/

struct Button btn1;
uint8_t read_btn1_gpio(void);
void btn1_callback(void *btn);

struct Button btn2;
uint8_t read_btn2_gpio(void);
void btn2_callback(void *btn);


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Button_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	BUTTON1_APBxClock_FUN(BUTTON1_APBxClock_CLK, ENABLE);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_InitStruct.GPIO_Pin  = BUTTON1_PIN ;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(BUTTON1_PORT, &GPIO_InitStruct);
	
	BUTTON2_APBxClock_FUN(BUTTON2_APBxClock_CLK, ENABLE);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_InitStruct.GPIO_Pin  = BUTTON2_PIN ;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(BUTTON2_PORT, &GPIO_InitStruct);
	
	
	
	button_init(&btn1, 	 read_btn1_gpio, 	BUTTON1_DOWN_LEVEL );
	button_attach(&btn1, SINGLE_CLICK,     	btn1_callback);
	button_attach(&btn1, DOUBLE_CLICK,     	btn1_callback);
	button_attach(&btn1, LONG_PRESS_START,  	btn1_callback);
    button_start(&btn1);
	
	button_init(&btn2, 	 read_btn2_gpio, 	BUTTON2_DOWN_LEVEL);
	button_attach(&btn2, SINGLE_CLICK,     	btn2_callback);
	button_attach(&btn2, DOUBLE_CLICK,     	btn2_callback);
	button_attach(&btn2, LONG_PRESS_START, 	btn2_callback);
    button_start(&btn2);
	

}

/*******菜单操作变量定义 start********/
uint8_t func_index = 0;
/*******菜单操作变量定义 end********/


uint8_t read_btn1_gpio(void)
{
	return GPIO_ReadInputDataBit(BUTTON1_PORT, BUTTON1_PIN);
	
}
void btn1_callback(void *btn)
{
	switch(get_button_event((struct Button*)btn))
	{
		case SINGLE_CLICK:
			printf("BUTTON1 SINGLE_CLICK! \r\n");
		#if MENU_ENABLE
		    func_index=menu_table[func_index].next;//按键next按下后的索引号	
			menu_table[func_index].current_operation();//执行当前操作函数
		#endif    
			break;
		
		case DOUBLE_CLICK:
			printf("BUTTON1 DOUBLE_CLICK! \r\n");
		
		#if ((MENU_ENABLE) && (LCD_ENABLE))
			if(func_index == 0) LCD_Clear(WHITE);
			func_index=menu_table[func_index].enter;
			menu_table[func_index].current_operation();//执行当前操作函数
		#endif	
		
		#if (!(MENU_ENABLE) && (LCD_ENABLE))
		    LCD_Clear(WHITE);
		    BACK_COLOR=WHITE;
			LCD_Test();
		#endif
			break;
		
		case LONG_PRESS_START:
			printf("BUTTON1 LONG_PRESS_START! \r\n");
		
		#if LCD_ENABLE
			LCD_Clear(WHITE);
		#endif
		
		#if ((MENU_ENABLE) && (LCD_ENABLE))
			func_index = 0;
			menu_table[func_index].current_operation();//长按回主页面
		#endif
		
		#if (!(MENU_ENABLE) && (LCD_ENABLE))
		    BACK_COLOR=WHITE;
			LCD_Test();
		#endif
		
			break;
		default:
			break;
		
	}

}	

uint8_t read_btn2_gpio(void)
{
	return GPIO_ReadInputDataBit(BUTTON2_PORT, BUTTON2_PIN);
}

void btn2_callback(void *btn)
{
	switch(get_button_event((struct Button*)btn))
	{
		
		case SINGLE_CLICK:
			printf("BUTTON2 SINGLE_CLICK! \r\n");
			break;
		case DOUBLE_CLICK:
			printf("BUTTON2 DOUBLE_CLICK! \r\n");
			break;
		case LONG_PRESS_START:
			printf("BUTTON2 LONG_PRESS_START! \r\n");
			break;
		default:
			break;
		
	}

}




#endif

/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

