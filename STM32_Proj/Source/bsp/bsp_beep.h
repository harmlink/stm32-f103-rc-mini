/*******************************************************************************
 * @file    bsp_beep.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_BEEP_H__
#define __BSP_BEEP_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_BEEP_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if BEEP_ENABLE

/* Exported constants --------------------------------------------------------*/



/* Exported types ------------------------------------------------------------*/


/* Exported macro ------------------------------------------------------------*/

#define  BEEP_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define  BEEP_APBxClock_CLK    	RCC_APB2Periph_GPIOC
#define BEEP_GPIO_PORT    		GPIOC
#define BEEP_GPIO_PIN     		GPIO_Pin_1

#define BEEP_ON_LEVEL   		1
#define BEEP_OFF_LEVEL  		0

#define BEEP_ON()  (BEEP_ON_LEVEL ? GPIO_SetBits(BEEP_GPIO_PORT,BEEP_GPIO_PIN):GPIO_ResetBits(BEEP_GPIO_PORT,BEEP_GPIO_PIN))
#define BEEP_OFF() (BEEP_OFF_LEVEL ? GPIO_SetBits(BEEP_GPIO_PORT,BEEP_GPIO_PIN):GPIO_ResetBits(BEEP_GPIO_PORT,BEEP_GPIO_PIN))

/* Exported functions --------------------------------------------------------*/
EXTERN void Beep_Init(void);
EXTERN void Beep_Control(uint8_t OnOff);
EXTERN void Beep_Task_Handler(void);
EXTERN void shell_beep_cmd(char argc, char *argv);

#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

