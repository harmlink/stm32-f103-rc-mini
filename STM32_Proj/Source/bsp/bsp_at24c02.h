/*******************************************************************************
 * @file    bsp_at24c02.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __AT24C02_H__
#define __AT24C02_H__

#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_AT24C02_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"
#include "bsp_i2c.h"


#if  AT24C02_ENABLE


/* Exported constants --------------------------------------------------------*/
#define EEPROM_DEV_ADDR  0xA0
#define EEPROM_WR        0x00
#define EEPROM_RD        0x01



/* Exported types ------------------------------------------------------------*/


/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
EXTERN void At24c02Init(void);
EXTERN void At24c02Write(unsigned char addr,unsigned char *buff,int size);
EXTERN void At24c02Read(unsigned char addr,unsigned char *buff,int size);

EXTERN void AT24C02Test(void);
#endif

#ifdef __cplusplus
}
#endif


#endif


