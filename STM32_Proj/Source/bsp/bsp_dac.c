/*******************************************************************************
 * @file    bsp_dac.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_DAC_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_dac.h"

#if DAC_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/




/* Private function prototypes -----------------------------------------------*/
static void DacGpioInit(void);
static void DacModeInit(void);
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Dac_Init(void)
{
	DacGpioInit();
	DacModeInit(); 
	
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Dac_SetVol(float vol)
{
	vol=(vol/3.3)*4095;
	DAC_SetChannel1Data(DAC_Align_12b_R,vol);
	DAC_SoftwareTriggerCmd(DAC_Channel_1, ENABLE);
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
uint16_t WaveData[128] ={
        2047,2147,2248,2347,2446,2545,2641,2737,2831,2922,
        3012,3100,3185,3267,3346,3422,3495,3564,3630,3692,
        3749,3803,3853,3898,3939,3975,4006,4033,4055,4072,
        4085,4092,4095,4092,4085,4072,4055,4033,4006,3975,
        3939,3898,3853,3803,3749,3692,3630,3564,3495,3422,
        3346,3267,3185,3100,3012,2922,2831,2737,2641,2545,
        2446,2347,2248,2147,2047,1947,1846,1747,1648,1549,
        1453,1357,1263,1172,1082, 994, 909, 827, 748, 672,
         599, 530, 464, 402, 345, 291, 241, 196, 155, 119,
          88,  61,  39,  22,   9,   2,   0,   2,   9,  22,
          39,  61,  88, 119, 155, 196, 241, 291, 345, 402,
         464, 530, 599, 672, 748, 827, 909, 994,1082,1172,
        1263,1357,1453,1549,1648,1747,1846,1947
};

void Dac_Task_Handler(void)
{
    
    static uint8_t WaveIndex = 0;

    Dac_SetVol(WaveData[WaveIndex]*3.3/4096.0);//需乘以系数3.3/4096，该函数参数为输出模拟电压值


    WaveIndex = (WaveIndex + 1) % 128;
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void DacGpioInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE );
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;				 
 	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN; 		 
 	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
 	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void DacModeInit(void)
{
	DAC_InitTypeDef DAC_InitType;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE );	
	DAC_InitType.DAC_Trigger = DAC_Trigger_Software;	
	DAC_InitType.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitType.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bits11_0;
	DAC_InitType.DAC_OutputBuffer = DAC_OutputBuffer_Enable;	
	DAC_Init(DAC_Channel_1,&DAC_InitType);	
	DAC_Cmd(DAC_Channel_1, ENABLE); 
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

