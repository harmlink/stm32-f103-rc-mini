/*******************************************************************************
 * @file    bsp_usart3.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_USART3_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_usart3.h"


#if USART3_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
uint8_t   g_USART3_RxBuf[USART3_RX_BUF_SIZE];//串口接收缓冲区
uint16_t  g_USART3_RecPos = 0;//存放当前串口接收数据存放的位置
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Usart3_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef  USART_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);//注意usart3为AP1总线外设

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_10 ;  //TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_11 ;  //RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	
	USART_InitStructure.USART_BaudRate = USART3_BAUD;  
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity =  USART_Parity_No;
	USART_InitStructure.USART_Mode = USART_Mode_Rx |USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART3, &USART_InitStructure);//初始化串口3
	
	//串口3中断初始化
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART3, ENABLE);//使能串口3
	
	USART_ClearFlag(USART3, USART_FLAG_TC  );//清发送完成标志位


}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
static void Usart3_SendByte(uint8_t ch)
{
	
	USART_SendData(USART3, (uint8_t)ch);
	
	while(USART_GetFlagStatus(USART3, USART_FLAG_TC)== RESET);
}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Usart3_SendString(uint8_t *str) //发送字符串
{
	uint32_t pos = 0;
	while(*(str+pos)!='\0')
	{
		Usart3_SendByte(*(str+pos));
		pos ++;
		
	}
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//USART3_IRQHandler,串口3中断回调函数
void USART3_IRQHandler(void)
{

	uint8_t RecCh;

	if( USART_GetFlagStatus(USART3,USART_FLAG_RXNE)!=RESET )				// 串口接收数据 
	{		
        
		Usart3RecTimer = 10;
		RecCh =(uint8)USART_ReceiveData(USART3);
		g_USART3_RxBuf[g_USART3_RecPos++] =RecCh;
        
		USART_ClearFlag(USART3, USART_FLAG_RXNE);
	}
	if( USART_GetFlagStatus(USART3,USART_FLAG_ORE)==SET ) 				// 串口溢出错误
	{
		USART_ClearFlag(USART3, USART_FLAG_ORE);
	}
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
unsigned char Usart3_RecProcess(void)
{
	
	if(Usart3RecTimer) return FALSE;
	if(!g_USART3_RecPos) return FALSE;
	
	g_USART3_RxBuf[g_USART3_RecPos]='\0';
	
/*********************收包处理  Begin***************/
//	"Beep On" 
//	"BeeP Off"
	if(strstr((char *)g_USART3_RxBuf,"Beep On")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_ON();
		#endif
	}
	else if(strstr((char *)g_USART3_RxBuf,"Beep Off")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_OFF();
		#endif
	}
/*********************收包处理  End ***************/
	//memset(g_USART3_RxBuf,0,USART3_RX_BUF_SIZE);
	g_USART3_RecPos = 0;
	
	return TRUE;
}

void Usart3Recv_Task_Handler(void)
{
	Usart3_RecProcess();
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

