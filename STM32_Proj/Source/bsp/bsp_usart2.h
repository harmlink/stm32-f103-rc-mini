/*******************************************************************************
 * @file    bsp_usart2.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_USART2_H__
#define __BSP_USART2_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_USART2_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if USART2_ENABLE

/* Exported constants --------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/



/* Exported macro ------------------------------------------------------------*/
#define USART2_BAUD         115200
#define USART2_RX_BUF_SIZE  256

/* Exported variables --------------------------------------------------------*/
EXTERN uint8_t   	g_USART2_RxBuf[USART2_RX_BUF_SIZE];//串口接收缓冲区
EXTERN uint16_t 	g_USART2_RecPos;//存放当前串口接收数据存放的位置

/* Exported functions --------------------------------------------------------*/

EXTERN void Usart2_Init(void);
EXTERN void Usart2_SendString(uint8_t *str);//发送字符串
EXTERN void Usart2Recv_Task_Handler(void);

#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

