/*******************************************************************************
 * @file    bsp_systick.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_SYSTICK_H__
#define __BSP_SYSTICK_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_SYSTICK_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"



/* Exported constants --------------------------------------------------------*/
#define MAX_SOFT_TIMER 8 //利用系统滴答定时器实现软件定时器的个数

/* Exported variables --------------------------------------------------------*/
extern uint32_t sysTimer[MAX_SOFT_TIMER];

/* Exported types ------------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/
#define DelayTimer 					    sysTimer[0]  //延时定时器
#define Usart1RecTimer 					sysTimer[1] //串口1接收定时器，如果超时，表示一帧接收完毕
#define Usart2RecTimer 					sysTimer[2]
#define Usart3RecTimer 					sysTimer[3]
#define Uart4RecTimer					sysTimer[4]
#define Uart5RecTimer					sysTimer[5]

/* Exported functions --------------------------------------------------------*/
EXTERN void SysTick_Init(void);
EXTERN void SysTick_DelayMs(uint32 nms);
EXTERN void SysTick_DelayUs(uint32 nus);



#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

