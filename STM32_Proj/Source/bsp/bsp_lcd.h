/*******************************************************************************
 * @file    bsp_lcd.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_LCD_H__
#define __BSP_LCD_H__	


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_LCD_C__
#define EXTERN
#else
#define EXTERN	extern
#endif

/* Includes ------------------------------------------------------------------*/
#include "config.h"
#include "stdlib.h"	   

#if LCD_ENABLE


/* Exported constants --------------------------------------------------------*/
#define LCD_W 240
#define LCD_H 240
#define	u8 unsigned char
#define	u16 unsigned int
#define	u32 unsigned long

//OLED模式设置
//0:4线串行模式
#define OLED_MODE 0
#define SIZE 16
#define XLevelL		0x00
#define XLevelH		0x10
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xFF 
#define X_WIDTH 	128
#define Y_WIDTH 	64	    						  
//-----------------OLED端口定义----------------  
//              GND    电源地
//              VCC  接5V或3.3v电源
//              D0   接CLK（SCL） PB0
//              D1   接MOSI（SDA） PB12
//              RES  接PB13
//              DC   接PB14
//              CS   接PB15 
#define  TFT_SCL_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define  TFT_SCL_APBxClock_CLK    	RCC_APB2Periph_GPIOB
#define TFT_SCL_PORT   GPIOB
#define TFT_SCL_PIN    GPIO_Pin_0

#define  TFT_SDA_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define  TFT_SDA_APBxClock_CLK    	RCC_APB2Periph_GPIOB
#define TFT_SDA_PORT   GPIOB
#define TFT_SDA_PIN    GPIO_Pin_12

#define  TFT_RES_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define  TFT_RES_APBxClock_CLK    	RCC_APB2Periph_GPIOB
#define TFT_RES_PORT   GPIOB
#define TFT_RES_PIN    GPIO_Pin_13

#define  TFT_DC_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define  TFT_DC_APBxClock_CLK    	RCC_APB2Periph_GPIOB
#define TFT_DC_PORT   GPIOB
#define TFT_DC_PIN    GPIO_Pin_14

#define  TFT_CS_APBxClock_FUN    	RCC_APB2PeriphClockCmd
#define  TFT_CS_APBxClock_CLK    	RCC_APB2Periph_GPIOB
#define TFT_CS_PORT   GPIOB
#define TFT_CS_PIN    GPIO_Pin_15

#define OLED_SCLK_Clr() GPIO_ResetBits(TFT_SCL_PORT,TFT_SCL_PIN)//CLK
#define OLED_SCLK_Set() GPIO_SetBits(TFT_SCL_PORT,TFT_SCL_PIN)

#define OLED_SDIN_Clr() GPIO_ResetBits(TFT_SDA_PORT,TFT_SDA_PIN)//SDA
#define OLED_SDIN_Set() GPIO_SetBits(TFT_SDA_PORT,TFT_SDA_PIN)

#define OLED_RST_Clr() GPIO_ResetBits(TFT_RES_PORT,TFT_RES_PIN)//RES
#define OLED_RST_Set() GPIO_SetBits(TFT_RES_PORT,TFT_RES_PIN)

#define OLED_DC_Clr() GPIO_ResetBits(TFT_DC_PORT,TFT_DC_PIN)//DC
#define OLED_DC_Set() GPIO_SetBits(TFT_DC_PORT,TFT_DC_PIN)
 		     
#define OLED_BLK_Clr()  GPIO_ResetBits(TFT_CS_PORT,TFT_CS_PIN)//BLK
#define OLED_BLK_Set()  GPIO_SetBits(TFT_CS_PORT,TFT_CS_PIN)

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据


//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE           	 0x001F  
#define BRED             0XF81F
#define GRED 			       0XFFE0
#define GBLUE			       0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			     0XBC40 //棕色
#define BRRED 			     0XFC07 //棕红色
#define GRAY  			     0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 
 
#define LIGHTGREEN     	 0X841F //浅绿色
#define LGRAY 			     0XC618 //浅灰色(PANNEL),窗体背景色

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)

/* Exported macro ------------------------------------------------------------*/
EXTERN  u16 BACK_COLOR;   //背景色

/* Exported functions --------------------------------------------------------*/
EXTERN void LCD_Writ_Bus(u8 dat);
EXTERN void LCD_WR_DATA8(u8 dat);
EXTERN void LCD_WR_DATA(u16 dat);
EXTERN void LCD_WR_REG(u8 dat);
EXTERN void LCD_Address_Set(u16 x1,u16 y1,u16 x2,u16 y2);
EXTERN void Lcd_Init(void); 
EXTERN void LCD_Clear(u16 Color);
EXTERN void LCD_ShowChinese(u16 x,u16 y,u8 index,u8 size,u16 color);
EXTERN void LCD_DrawPoint(u16 x,u16 y,u16 color);
EXTERN void LCD_DrawPoint_big(u16 x,u16 y,u16 colory);
EXTERN void LCD_Fill(u16 xsta,u16 ysta,u16 xend,u16 yend,u16 color);
EXTERN void LCD_DrawLine(u16 x1,u16 y1,u16 x2,u16 y2,u16 color);
EXTERN void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2,u16 color);
EXTERN void Draw_Circle(u16 x0,u16 y0,u8 r,u16 color);
EXTERN void LCD_ShowChar(u16 x,u16 y,u8 num,u8 mode,u16 color);
EXTERN void LCD_ShowString(u16 x,u16 y,const u8 *p,u16 color);
EXTERN u32 mypow(u8 m,u8 n);
EXTERN void LCD_ShowNum(u16 x,u16 y,u16 num,u8 len,u16 color);
EXTERN void LCD_ShowNum1(u16 x,u16 y,float num,u8 len,u16 color);
EXTERN void LCD_ShowPicture(u16 x1,u16 y1,u16 x2,u16 y2);
EXTERN void LCD_Test(void);
#endif

#ifdef __cplusplus
}
#endif

#endif  
	 
	 



