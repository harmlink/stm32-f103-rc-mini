/*******************************************************************************
 * @file    bsp_usart2.c
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#define __BSP_USART2_C__


/* Includes ------------------------------------------------------------------*/
#include "bsp_usart2.h"


#if USART2_ENABLE

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


/* Exported variables --------------------------------------------------------*/
uint8_t   g_USART2_RxBuf[USART2_RX_BUF_SIZE];//串口接收缓冲区
uint16_t  g_USART2_RecPos = 0;//存放当前串口接收数据存放的位置
/* Exported function prototypes ----------------------------------------------*/


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Usart2_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	USART_InitTypeDef  USART_InitStructure;
	NVIC_InitTypeDef   NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);//注意usart2为AP1总线外设

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_2 ;  //TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_3 ;  //RX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	USART_InitStructure.USART_BaudRate = USART2_BAUD;  
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity =  USART_Parity_No;
	USART_InitStructure.USART_Mode = USART_Mode_Rx |USART_Mode_Tx;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART2, &USART_InitStructure);//初始化串口2
	
	//串口1中断初始化
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;  
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	USART_Cmd(USART2, ENABLE);//使能串口2
	
	USART_ClearFlag(USART2, USART_FLAG_TC  );//清发送完成标志位


}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
static void Usart2_SendByte(uint8_t ch)
{
	
	USART_SendData(USART2, (uint8_t)ch);
	
	while(USART_GetFlagStatus(USART2, USART_FLAG_TC)== RESET);
}
/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
void Usart2_SendString(uint8_t *str) //发送字符串
{
	uint32_t pos = 0;
	while(*(str+pos)!='\0')
	{
		Usart2_SendByte(*(str+pos));
		pos ++;
		
	}
}


/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
//USART2_IRQHandler,串口2中断回调函数
void USART2_IRQHandler(void)
{

	uint8_t RecCh;

	if( USART_GetFlagStatus(USART2,USART_FLAG_RXNE)!=RESET )				// 串口接收数据 
	{		
        
		Usart2RecTimer = 10;
		RecCh =(uint8)USART_ReceiveData(USART2);
		g_USART2_RxBuf[g_USART2_RecPos++] =RecCh;
        
		USART_ClearFlag(USART2, USART_FLAG_RXNE);
	}
	if( USART_GetFlagStatus(USART2,USART_FLAG_ORE)==SET ) 				// 串口溢出错误
	{
		USART_ClearFlag(USART2, USART_FLAG_ORE);
	}
}

/*******************************************************************************
 * @brief       
 * @param       
 * @retval      
 * @attention   
*******************************************************************************/
unsigned char Usart2_RecProcess(void)
{
	
	if(Usart2RecTimer) return FALSE;
	if(!g_USART2_RecPos) return FALSE;
	
	g_USART2_RxBuf[g_USART2_RecPos]='\0';
	
/*********************收包处理  Begin***************/
//	"Beep On" 
//	"BeeP Off"
	if(strstr((char *)g_USART2_RxBuf,"Beep On")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_ON();
		#endif
	}
	else if(strstr((char *)g_USART2_RxBuf,"Beep Off")!= NULL)
	{
		#if BEEP_ENABLE
			BEEP_OFF();
		#endif
	}
/*********************收包处理  End*****************/
	//memset(g_USART2_RxBuf,0,USART2_RX_BUF_SIZE);
	g_USART2_RecPos = 0;
	
	return TRUE;
}

void Usart2Recv_Task_Handler(void)
{
	Usart2_RecProcess();
}

#endif
/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

