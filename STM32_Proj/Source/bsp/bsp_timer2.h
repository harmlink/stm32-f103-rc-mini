/*******************************************************************************
 * @file    bsp_timer2.h
 * @author  Harm
 * @version V1.00
 * @date    15-May-2022
 * @brief   ......
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_TIMER2_H__
#define __BSP_TIMER2_H__


#ifdef __cplusplus
extern "C" {
#endif


#undef  EXTERN


#ifdef  __BSP_TIMER2_C__
#define EXTERN
#else
#define EXTERN	extern
#endif


/* Includes ------------------------------------------------------------------*/
#include "config.h"

#if TIMER2_ENABLE

/* Exported constants --------------------------------------------------------*/


/* Exported types ------------------------------------------------------------*/


/* Exported macro ------------------------------------------------------------*/


/* Exported functions --------------------------------------------------------*/
EXTERN void Timer2_Init(int prescaler,int period);


#endif

#ifdef __cplusplus
}
#endif


#endif


/******************* (C) COPYRIGHT 2022 END OF FILE****************************/

